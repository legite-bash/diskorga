#!/bin/bash
# creer un repertoire /tmp/test/renommer
# avec des repertoires et fichiers test


declare -r SCRIPT_PATH=$(realpath $0);      # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=${SCRIPT_PATH%/*};    # repertoire absolu du script (pas de slash de fin)
VERSION="v0.0.3-2021.07.22";

if [ ! -f "$SCRIPT_REP/legite-lib.sh" ]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit"
    exit 1
fi
source $SCRIPT_REP/legite-lib.sh



    # VARIABLES GENERALES
    declare -r diskorga_bin="/www/bash/diskorga/diskorga.sh";
    declare -r TEST_ROOT="/dev/shm/diskorga";
    declare   testSrcDir="$TEST_ROOT/src";
    declare   testDestDir="$TEST_ROOT/dest";


##################
# INITIALISATION #
##################
        # - Creation des templates de tests - #
        function createFichiers(){
            fctIn "$*"

            for _fichierName in ${_dirPrefix}F{1..10}
            do
                touch "$_fichierName"
            done

            fctOut "$UNCNAME";return 0
        }


        function createRep9(){
            fctIn "$*"
            local -i _dirIndex=${1:-1}
            local    _dirPrefixe="${2:-"D$_dirIndex"}"
            if [ $_dirIndex -ge 9 ]; then return 0;fi
            
            #displayVar '_dirIndex' "$_dirIndex" '_dirPrefixe' "$_dirPrefixe"
            mkdir "$_dirPrefixe"
            cd "$_dirPrefixe"
            if [ $? -ne 0 ]; then fctOut "$UNCNAME";return $E_INODE_NOT_EXIST;fi


            (( _dirIndex++ ))
            _dirPrefixe="${_dirPrefixe}D$_dirIndex"
            createRep9 $_dirIndex "$_dirPrefixe"

            createFichiers

            cd ..

            fctOut "$UNCNAME";return 0
        }


        function createDirRecursif9(){
            fctIn;
            local _dirPrefixe9="${1:-""}"
            for _dirIndex9 in {1..9}
            do
                createRep9 $_dirIndex9 "D$_dirIndex9"
            done
            fctOut "$UNCNAME";return 0;
        }


    # crait le repertoire de test
    function initRepTest(){
        fctIn "$*"
        displayVar 'testSrcDir' "$testSrcDir"
        if [ -z "$testSrcDir" ]; then erreursAddEcho '$testSrcDir VIDE: on arrete'; fctOut "$UNCNAME";exit E_INODE_NOT_EXIST;fi
        if [ -d "$testSrcDir" ]; then rm -R "$testSrcDir"/*; fi
        displayVar "testSrcDir" "$testSrcDir";

        mkdir -p "$testSrcDir" && cd "$testSrcDir"
        if [ $? -ne 0 ]
        then
            erreursAddEcho "Erreur lors de la creation de $testSrcDir";
            endStandard
            exit $E_INODE_NOT_EXIST
        fi
        displayVar 'REP COURANT' "$PWD"

        createFichiers
        createDirRecursif9


        if [ $? -ne 0 ]
        then
            erreursAddEcho 'Création des repertoires et fichiers de tests'
        else
            notifsAddEcho 'Création des repertoires et fichiers de tests'
        fi
        fctOut "$UNCNAME";return 0;
    }


    function initialisation(){
        fctIn "$*"

        titre1 '# INITIALISATION #'

        initRepTest
        #touch "a+b.ext"
        #touch "a b .ext"
        #touch "a [b].ext"

        #test_filtre_supprimerFichiersParasite
        #touch "desktop.ini"
        #touch "Thumbs.db"
        #touch "AlbumArt-ex AlbumArt"
        #touch "AlbumArtAlbumArt"
        #touch -- "-AlbumArtAlbumArt"
        #mkdir 'rep avec espace'
        #touch 'rep avec espace/fichier avec espace'
        fctOut "$UNCNAME";return 0;
    }
#

######################################
function diskorga_test(){
    fctIn "$*"

    titre1 'LES TESTS'
    #evalCmd "$diskorga_bin "
    #evalCmd "$diskorga_bin --showLibs"


    test_stats_analyse

    #test_rename
    #test_copy
    fctOut "$UNCNAME"; return 0;
}


###################
## STATS_ANALYSE ##
###################
    function test_stats_analyse(){
        fctIn "$*"
        titre1 'TESTS ANALYSE DE REPERTOIRE'

        diskorga.sh '/home'
        diskorga.sh '/home' '/home'
        diskorga.sh '/home' '/etc'

        fctOut "$UNCNAME"; return 0;
    }
#

##############
## RENOMMER ##
##############
    function test_rename(){
        fctIn "$*"
        tirre1 'TESTS DE RENAME'
        initRepTest "$TEST_ROOT/rename"

        #test_rename_carSpeciaux
        #test_rename_ecraser
        
        fctOut "$UNCNAME"; return 0;
    }

    function test_rename_carSpeciaux(){
        fctIn "$*"
        titre3 "$FUNCNAME"
        initRepTest "$TEST_ROOT/rename/carSpeciaux"
        #evalCmd "$diskorga_bin -d --rename $testSrcDir "
        touch "$testSrcDir/carSpeciaux-{[|@]}"
        touch "$testSrcDir/carSpe[iaux"

        test_rename_ecraser

        # lancer les tests de rename
        #evalCmd "$diskorga_bin -d --rename $testSrcDir "
        evalCmd "$diskorga_bin -R --rename $testSrcDir "

        fctOut "$UNCNAME"; return 0;

    }

    function test_rename_ecraser(){
        fctIn "$*"
        titre2 $FUNCNAME': cas de l ecrasement'
        initRepTest "$TEST_ROOT/rename/ecrasement"

        # renommer: cas de l'ecrasement d'un fichier existant
        echo 'fichier qui ecrase'      > 'fichier qui existe'
        echo 'fichier qui sera ecrasé' > 'fichier_qui_existe'
        displayVar 'fichier qui existe'  "$(cat 'fichier qui existe')"
        displayVar 'fichier_qui_existe'  "$(cat 'fichier_qui_existe')"

        evalCmd "$diskorga_bin  --rename $testSrcDir"

        #echo 'Resultat:'
        displayVar 'fichier qui existe'  "$(cat 'fichier qui existe')"
        displayVar 'fichier_qui_existe'  "$(cat 'fichier_qui_existe')"

        fctOut "$UNCNAME"; return 0;
    }
#


############
## COPIER ##
############
    function test_copy(){
        fctIn "$*"
        titre1 'TESTS DE COPIE'

        titre2 'Test copie de repertoire uniquement'
        #evalCmd "$diskorga_bin -d -R --createCache  $testSrcDir $testDestDir"
         evalCmd "$diskorga_bin -d -R --keepCache --dironly  --copy  $testSrcDir $testDestDir"
        #evalCmd "$diskorga_bin -d -R --keepCache   --copy  $testSrcDir $testDestDir"

        #titre2 'Test copie de fichiers '
        #evalCmd "$diskorga_bin -f -R --copy  $testSrcDir $testDestDir"

        fctOut "$UNCNAME"; return 0;
    }
#



# ########## #
# PARAMETRES #
# ########## #
TEMP=$(getopt \
     --options v::dVhnpRc:: \
     --long help,version,verbose::,debug,showVars,showCollections,update,conf::,showLibs,showDuree\
,profondeur_max::,createTest,test,dironly,fileonly,rename,copy,compare,convert,convertFrom::,convertTo::,stats,createCache,keepCache,fromCache,cachePrefixe::,fichierMotif:: \
     -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"
        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                shift ;;
            -V|--version) echo "$VERSION"; exit 0;      shift ;;
            -v|--verbose)
                case "$parametre" in
                    '') verbosity=1; shift 2 ;;
                    *)  #echo "Option c, argument '$2'" ;
                    verbosity=$parametre; shift 2;;
                esac
                ;;
            -d|--debug) ((isDebug++));isShowVars=1;         shift  ;;
            --update)   isUpdate=1;                     shift ;;

            --showVars)         isShowVars=1;           shift  ;;
            --showCollections)  isShowCollections=1;    shift  ;;
            --showLibs )        isShowLibs=1;           shift  ;;
            --showDuree )       isShowDuree=1;          shift  ;;

            --conf)     CONF_PSP_PATH="$parametre";  shift 2; ;;

            -S)
                isSimulation=1;
                YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
                shift
                ;;

            --collection)       collection=$parametre; shift 2 ;;


            # - parametres liés au projet - #

    
            --fichierMotif)
                #fichierMotif="\"$2\"";
                fichierMotif="$2"
                shift 2
                ;;

            # - Les parametres supplementaires - #
            --)
                repertoire_source="${2:-"./"}"
                repertoire_destination=${3:-"./"}
                shift;
                break
                ;;
            
            *)
                repertoire_source="${1:-"./"}"
                repertoire_destination=${2:-"./"}
                shift;
                break
        esac
    done
#

### EXECUTION DES TESTS ###
initialisation
diskorga_test

ls -l $testSrcDir
titre4 'FIN NORMAL'
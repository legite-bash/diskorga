#!/bin/bash
set -u;


#normalizeText global (legite-lib.sh)
#renommerInodeLocal(){
#    showDebug "diskorga.ori|cfg.sh $FUNCNAME($@)";
#    echo "diskorga.ori|cfg.sh $FUNCNAME($@)";
#    #normalizeText=$(echo $normalizeText | sed -e 's/Text a changé/Test de rempacement/g');
#}

###############
# les filtres #
###############
##########################################
# filtre_supprimerFichierParasite(@)     #
# $@: tous les arg                       #
# utilise la variable globale $isDeleted #
##########################################
addLibrairie 'filtre_supprimerFichiersParasite'
function filtre_supprimerFichiersParasite(){
    fctIn "diskorga.ori.sh $FUNCNAME($*)";
    #oriName="$@";
    local isDeleted=0;
        #"Desktop.ini" | "desktop.ini" | "Thumbs.db" | "thumbs.db" )
    case "${inode,,}" in 
        "desktop.ini" | "thumbs.db" )
            isDeleted=1;
            ;;
    esac

    # suppression des AlbumArt*
    subAlbumArt=${inode:0:9};
    displayVarDebug 'inode' "$inode" 'subAlbumArt' "$subAlbumArt"
    if [ "$subAlbumArt" = "AlbumArt" ]
    then
        isDeleted=1;
    fi

    if [ "$isDeleted" -eq 1 ]
    then
        echo "${tabulation}${WARN}Suppression de $inode.$NORMAL";
        rm "./$inode";
        ((inodeCompteur['fichiers_supprimes']++));
    fi
    fctOut; return 0;
}


addLibrairie 'filtre_supprimerSignatures'
function filtre_supprimerSignatures(){
    fctIn "$SOURCE_BASH: $LINENO: $FUNCNAME($*)"
    #if [ $# -eq 0 ];then return $E_ARG_REQUIRE; fi

    #normalizeText="$1";

     # - SUPPRIMER LES SEQUENCES SUIVANTES - #
    normalizeText=$(echo $normalizeText | sed -e 's/emule-island.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/emule-island.ru//I');
    normalizeText=$(echo $normalizeText | sed -e 's/zone-telechargement.ws//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Annuaire-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement.net//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement.Ws//I');
    normalizeText=$(echo $normalizeText | sed -e 's/zone-telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/Annuaire-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Annuaire-Telechargement.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement1.org//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Annuaire.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Zone-Telechargement1.com//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.Cpasbien.me//I');
    normalizeText=$(echo $normalizeText | sed -e 's/www.CpasBien.io//I');
    normalizeText=$(echo $normalizeText | sed -e 's/found_via_ed2k-series.new//I');
    normalizeText=$(echo $normalizeText | sed -e 's/.WwW.ZT-ZA.COM//I')
    normalizeText=$(echo $normalizeText | sed -e 's/.WwW.ZoNe-TelecharGement.CaM//gi')
    displayVarDebug "diskorga.ori: $LINENO: normalizeText" "$normalizeText"
    fctOut "$FUNCNAME";return 0;

}



#!/bin/bash

if [[ ${IS_LEGITELIB_CALL:-0} -eq 0 ]]
then
    echo "Ce fichier n'est pas destiné à être chargé directement.";
    echo "La librairie legite-lib.sh n'est pas chargé.";
    exit 1;
fi

source "${SCRIPT_REP}/${SCRIPT_NAME}.vars.sh";



# ################### #
# FONCTIONS DU PROJET #
# ################### #
function titreStats(){
    echo -e $MAGENTA"====  ${1}  ===="$NORMAL
}

#

###############################
# renommerInode("repertoire") #
###############################
    function renommerInode(){
        fctIn "$*"

        #http://www.tux-planet.fr/renommer-le-nom-des-fichiers-en-minuscule-sous-linux/

        if [[ $# -eq 0 ]]
        then
            erreursAddEcho "$FUNCNAME: Neccessite un repertoire en parametre."
            fctOut "$FUNCNAME";return $E_ARG_REQUIRE;
        fi
        oriName="$1";
        oldName="$oriName";

        filtreNo=0;

        if [[ $isTrapCAsk -eq 1 ]]
        then
            fctOut "$FUNCNAME";return 0;
        fi

        getNormalizeText "$oriName"

        # executer tous les filtres
        for filtreNom in ${filtres[*]}
        do
            ${filtres[$filtreNom]} #exucute la fonction filtre
        done


        renommerInodeLocal #diskorga.ori|cfg.sh
        newName="$normalizeText"


        if [[ "$oriName" == "$newName" ]]
        then
            if [[ -f "$newName" ]]
            then
                echo "${tabulation}\"$oriName\"";
            fi
        else

            # rename
            #echo "mv --no-clobber \"$oldName\" \"$newName\" 2>/dev/null"
            mv --no-clobber "$oldName" "$newName" 2>/dev/null
            local result=$?
            #echo "copie result=$result"


            if [[ -d "$newName" ]]
            then
                echo "${tabulation}$CYAN\"$oriName\"";
                echo "${tabulation}\"$newName\"$NORMAL";
                #echo ${tabulation}$YELLOW"filtres: $filtresActifs"$NORMAL;
                ((inodeCompteur['repertoires_modifies']++));
            fi

            if [[ -f "$newName" ]]
            then
                echo "${tabulation}$CYAN\"$oriName\"";
                echo "${tabulation}\"$newName\"$NORMAL";
                #echo ${tabulation}$YELLOW"filtres: $filtresActifs"$NORMAL;
                ((inodeCompteur['fichiers_modifies']++));
            fi
        fi
        fctOut "$FUNCNAME";return 0;
    }


    function setTabulationDl(){
        tabulation='';
        for (( t=1; t<=profondeurNb; t++ ))
        do
            tabulation="$tabulation$tabulationMotif"
        done
        tabulation=$INFO"$tabulation"$NORMAL
    }


    #moveFile(oldName, newName)
    function moveFileOld(){
        fctIn "$*"
        oldName="$1"; # global
        newName="$2"; # global
        #displayDebug "filtre $filtreNo:oldName=$oldName -> newName=$newName" $LINENO;

        if [[ "$oldName" != "$newName" ]]
        then
            #filtresActifs="$filtresActifs '$filtreNo'"
            mv --no-clobber "$oldName" "$newName" 2>/dev/null
            local result=$?
            echo "result=$result"
            #if [[ $result -eq 0 ]]
            #then
            #    echo "OK"
            #else
            #    echo "NOK"    
            #fi

            oldName="$newName";
        fi
        fctOut "$FUNCNAME";return 0;
    }
#


##############
# renameRep #
##############
    function renameRep(){
        fctIn "$*"

        local repertoireLocal="$1";
        if [[ ! -d "$repertoireLocal" ]]
        then
            erreursAddEcho "$repertoireLocal n'est pas un repertoire"
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi
    
        if [[ ! -w "$repertoireLocal" ]]
        then
            erreursAddEcho "$repertoireLocal n'est pas en mode écriture"
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi

        echo "${tabulation}${INFO}$repertoireTotal[$profondeurNb/$PROFONDEUR_MAX]$repertoireLocal)$NORMAL";

        ###################################################
        # - on se positionne dans le repertoire indiqué - #
        ###################################################
        cd "$repertoireLocal";
        local -i cd_cr=$?; #code retour de cd
        
        if [[ "$cd_cr" -ne 0 ]]
        then
            # si le repertoire a pas été changé on quitte la fonction
            echo "${tabulation}${WARN}Problème lors de l'entrée dans ce repertoire"
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi

        ((profondeurNb++));
        setTabulationDl

        # - gestion du niveau de recursivité - #
        if [[ $profondeurNb > $PROFONDEUR_MAX ]]
        then
            erreursAddEcho "${tabulation}${WARN}Niveau max ($profondeurNb/$PROFONDEUR_MAX) de sous repertoire atteint $NORMAL$NOBOLD";
            return # on sort de la fonction
        fi

        ((repertoireTotal++));
        ((inodeCompteur['repertoires_lus']++));

        #displayDebug "rep courant($profondeurNb):$PWD" $LINENO;
        # - analyse du contenu du repertoire - #
        for inode in *
        do
            if [[ $isTrapCAsk -eq 1 ]]
            then
                #displayDebug "$FUNCNAME: Interuption par Ctrl C"
                break;
            fi

            #displayVarDebug "$LINENO: PWD" "$PWD"  "inode" "$inode" ;
            if [[ -d "$inode" ]]
            then
                if [[ $isRecursif -eq 1 ]]
                then
                    repertoireActuel=$inode;
                    #repertoireLocal="$repertoireLocal/$inode/";
                    repertoireLocal="$PWD/$inode/";
                    renameRep "$repertoireLocal"
                fi

            elif [[ -f "$inode" ]]
            then
                displayDebug "on traite l'inode." $LINENO;
                ((inodeCompteur['fichiers_lus']++));

                # on supprime les fichiers parasites
                if [[ $isDironly -eq 0 ]]
                then
                    filtre_supprimerFichiersParasite
                fi

                if [[ $isDeleted -eq 1 ]]
                then
                    continue;
                fi

                if [[ $isDironly -eq 0 ]]
                then
                    renommerInode "$inode"
                fi

            elif [[ -L "$inode" ]]
            then
                echo "$INFO lien symbolique. On ne modifie pas.$NORMAL"
                ((inodeCompteur['liens']++));
            fi

        done

        # - on change le nom du repertoire lui meme - #
        #echo "$LINENO:ON RENOMME LE REPERTOIRE:$repertoireLocal";
        if [[ $isFileonly -eq 0 ]]
        then
            renommerInode "$repertoireLocal";
        fi

        # - retour au repertoire précédent - #
        #displayDebug "cd .." $LINENO;
        cd ..

        ((profondeurNb--));
        repertoireLocal="$PWD";

        setTabulationDl;
        fctOut "$FUNCNAME"; return 0;
    }
#


##########
# copier #
##########
    function copier(){
        fctIn "$*"
        titre1 'OPERATION DE COPIE'

        # on rajoute le slashe de fin (suppression si existant avant pour eviter un doublon)
        #repertoire_source=${repertoire_source%/}
        #repertoire_source="$repertoire_source/"
        #repertoire_destination=${repertoire_destination%/}
        #repertoire_destination="$repertoire_destination/"

        #echo "repertoire_source:$repertoire_source"
        #echo "repertoire_destination:$repertoire_destination"

        if [[ ! -d "$repertoire_source" ]]
        then
            erreursAddEcho "$UNCNAME: Le repertoire source: '$repertoire_source' n'est pas un repertoire"
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi

        if [ "$repertoire_source" == "$repertoire_destination" ]
        then
            erreursAddEcho "Les repertoires source et destination sont identiques: '$repertoire_source'"
            fctOut "$FUNCNAME"; return $E_ARG_BAD;
        fi

        # NE PAS CREER LE REPERTOIRE DE DESTINATION

        createCache 'd'
        copieRep 

        createCache 'f'
        copieFile
        fctOut "$FUNCNAME";return 0;
    }

    #createCache($type)
    function createCache(){
        fctIn "$*"
        if [[ $# -ne 1 ]]
        then
            fctOut "$FUNCNAME";return $E_ARG_REQUIRE;
        fi

        if [[ $isFromCache -eq 1 ]]; then fctOut "$FUNCNAME";return 0; fi

        local type="$1"

        case "$type" in
            'd')
                titre2 'Creation du cache des repertoires'
                if [ $isFileonly -eq 1 ] || [ $isRecursif -eq 0 ]
                then
                    notifsAddEcho "Creation du cache des repertoires désactivé"
                    displayVarDebug 'isFileonly' "$isFileonly" 'isRecursif' "$isRecursif"
                    setCodeRetour $E_F1 'Creation du cache des repertoires désactivé';
                    fctOut "$FUNCNAME"; return $codeRetour;
                fi
                cacheName="$cachePrefixe-dir.txt"
                ;;
            'f')
                titre2 'Creation du cache des fichiers'
                #displayVarDebug 'isDironly' "$isDironly"
                if [[ $isDironly -eq 1 ]]
                then
                    titreInfo 'La Création du cache des fichiers est désactivé (--dironly)'
                    displayVarDebug 'isDironly' "$isDironly" 
                    setCodeRetour $E_F2 'La Création du cache des fichiers est désactivé (--dironly)';
                    fctOut "$FUNCNAME"; return $codeRetour; 
                fi

                cacheName="$cachePrefixe-file.txt"
                displayVarDebug 'cacheName' "$cacheName"

            ;;
            * )
                erreursAddEcho "Mauvais type de cache"
                displayVarDebug 'type' "$type" 
                setCodeRetour $E_F5 'Mauvais type de cache';
                fctOut "$FUNCNAME"; return $codeRetour;

        esac

        displayDebug "Construction EFFECTIVE du cache avec le motif: '$fichierMotif'"
        if [[ "$fichierMotif" == '' ]]
        then
            displayDebug "find \"$repertoire_source\" -type $type"
            find "$repertoire_source" -type $type 2>/dev/null                          > "$cacheName"
        else
            displayDebug "find \"$repertoire_source\" -type $type | grep \"$fichierMotif\""
            find "$repertoire_source" -type $type | grep "$fichierMotif" 2>/dev/null   > "$cacheName"
        fi
        
        if [[ ! -f "$cacheName" ]]
        then
            erreursAddEcho "Création du cache des fichiers"
            setCodeRetour $E_F2 'Erreur lors de la création du cache des fichiers';
            fctOut "$FUNCNAME"; return $codeRetour;
        fi

        local inodeNb=$(wc --lines $cacheName);

        case "$type" in
            'd')
                inodeRepTotal=${inodeNb% *};
                ((inodeRepTotal--));
                ;;
            'f')
                inodeFicTotal=${inodeNb% *};
                ((inodeFicTotal--));

                # si pas recursif alors suppression des sous elements
                if [[ $isRecursif -eq 0 ]]
                then
                    echo "Suppression des sous elements"
                fi
                ;;
        esac

        echo "$cacheName crée avec $inodeRepTotal répertoires et $inodeFicTotal fichiers";
        #ls -l "$cacheName"


        ## -- trie du cache -- ##
        titre4 'trie du cache'
        readarray fichierList < "$cacheName"
        IFS='\n';
        #local _donneesTriees=($(sort --ignore-case <<< "${fichierList[*]}")) # les parentheses crée une liste
        local _donneesTriees=$(sort --ignore-case <<< "${fichierList[*]}")
        IFS=$IFS_DEFAULT
        unset fichierList

    
        #titre4  "Contenu du cache trié:$cacheName"
        #cat $cacheName

        # -- copie du tableau dans $cacheName -- #
        displayDebug "copie du tableau dans $cacheName" 1
        for inode in "${_donneesTriees[@]}"
        do
            echo "$inode" > "$cacheName.tri"
        done
        #evalCmdDebug "ls -l $cacheName.tri" $LINENO '' 2

        
        if [[ $isDebug -ge 3 ]]
        then
            titre4 "Contenu du cache:$cacheName.tri"
            cat $cacheName.tri
        fi
        ((actionNb++))
        fctOut "$FUNCNAME";return 0;
    }


    # Copie des REPERTOIRES #
    function copieRep(){
        fctIn "$*"
        # copie des sous repertoires uniquement si la recursivité est demandé
        if [ $isRecursif -eq 1 -a $isFileonly -eq 0 ]
        then
            titre2 "Copie des REPERTOIRES '$repertoire_source' vers '$repertoire_destination'"

            titre3 'Chargement du cache des repertoires triés'
            local _cacheName="$cachePrefixe-dir.txt";
            local _cacheNameTri="$_cacheName.tri";
            displayVarDebug '_cacheName' "$_cacheName" '_cacheNameTri' "$_cacheNameTri"

            # Creation du cache (si inexistant)
            if [[ ! -f "$_cacheNameTri" ]]
            then
                createCache 'd'
                if [[ $? -ne 0 ]]
                then
                    erreursAddEcho 'Cache: Erreur lors de la creation du cache ($_cacheName) -> Sortie de la copie'
                    fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
                fi
            fi

            titre3 'Chargement du cache trié'$_cacheNameTri
            local _donneesTriees
            readarray _donneesTriees < "$_cacheNameTri"

            ## -- creation du repertoire de destination -- ##
            ## SUPPRIMER CAR IL EST DANS LE FICHIER CACHE et que mkdir -p
            #if [[ ! -d "$repertoire_destination" ]]
            #then
            #    displayDebug 'Repertoire de destination inexistant -> création'
            #    evalCmd "mkdir -p \"$repertoire_destination\""
            #    if [[ $? -ne 0 ]]
            #    then
            #        erreursAddEcho 'Impossible de créer le repertoire de destination:'$repertoire_destination
            #        fctOut "$FUNCNAME"; return $E_INODE_NOT_EXIST;
            #    fi
            #fi


            titre3 'Copies des répertoires'
            local repertoire;
            local repertoire_source_relatif='';
            local repertoire_final='';

            for repertoire in "${_donneesTriees[@]}"
            do
                if [ $isTrapCAsk -eq 1 ]
                then
                    displayDebug "$FUNCNAME: Interuption par Ctrl C"
                    break;
                fi

                repertoire=$(echo $repertoire | tr -d '\n'); # supprime les sauts de ligne
                if [ -z "$repertoire" ];then continue; fi

                if [[ "${repertoire:0:1}" == '\' ]];then repertoire="${repertoire:1}"; fi # suppression du slash de debut

                #displayVarDebug "[$LINENO]:repertoire" "$repertoire"

                repertoire_source_relatif="${repertoire/$repertoire_source}"  # suppression de la source root
                repertoire_source_relatif="${repertoire_source_relatif#/}"    # suppression du slashe root
                #echo repertoire_source_relatif="$repertoire_source_relatif"

                repertoire_final="$repertoire_destination$repertoire_source_relatif"
                #echo "repertoire_final=$repertoire_final"


                if [ -d "$repertoire_final" ]
                then
                    ((inodeCompteur['repertoires_copies_existant']++))
                    ((inodeRepLu++));inodeRepPct=$((inodeRepLu * 100 / inodeRepTotal))
                    echo "[$inodeRepPct%]'$repertoire_final' existe deja."

                else
                    mkdir -p "$repertoire_final" 2>$cachePrefixe-cperror-last
                    local -i result=$?
                    if [ $result -eq 0 ]
                    then
                        ((inodeCompteur['repertoires_copies']++));
                        ((inodeRepLu++));inodeRepPct=$((inodeRepLu * 100 / inodeRepTotal))
                        notifsAddEcho "[$inodeRepPct%] mkdir -p '$repertoire_final'"; #  > 100% -> corriger le bug
                    else
                        local error=$(cat $cachePrefixe-cperror-last)
                        ((inodeCompteur['repertoires_non_copies']++));
                        erreursAddEcho "[$inodeRepPct%] mkdir -p '$repertoire_final': $error";
                    fi
                fi
            done

            if [ $isKeepCache -eq 0 ]
            then
                titre3 'Suppression des caches'
                if [ -f "$_cacheName" ];     then evalCmd "rm '$_cacheName'";fi
                if [ -f "$_cacheNameTri" ];  then evalCmd "rm '$_cacheNameTri'";fi
            fi

            ((actionNb++))

        fi #  if [ $isRecursif -eq 1 -a $isFileonly -eq 0 ]
        fctOut "$FUNCNAME";return 0;
    } #copieRep()


    # Copie des FICHIERS #
    function copieFile(){
        fctIn "$*"
        if [ $isDironly -eq 0 -a $isCreateCache -eq 0 ]
        then
            titre2 "Copie des FICHIERS '$repertoire_source' vers '$repertoire_destination'"

            ## - Chargement du cache - ##
            local _cacheName="$cachePrefixe-file.txt";
            local _cacheNameTri="$_cacheName.tri";
            displayVarDebug '_cacheName' "$_cacheName" '_cacheNameTri' "$_cacheNameTri"

            if [[ ! -f "$_cacheNameTri" ]]
            then
                createCache 'f'
                if [[ $? -ne 0 ]]
                then
                    erreursAddEcho 'Cache: Erreur lors de la creation du cache ($_cacheNameTri) -> Sortie de la copie'
                    fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
                fi
            fi


            titre3 'Creation du repertoire de destination'
            if [[ ! -d "$repertoire_destination" ]]
            then
                displayDebug "Repertoire de destination inexistant -> création"
                evalCmd "mkdir -p \"$repertoire_destination\""
                if [[ $? -ne 0 ]]
                then
                    erreursAddEcho 'Impossible de créer le repertoire de destination:'$repertoire_destination
                    fctOut "$FUNCNAME"; return $E_INODE_NOT_EXIST;
                fi
            fi

            titre3 'Chargement du cache des fichiers triés'
            local _donneesTriees
            readarray _donneesTriees < "$_cacheName.tri"

            if [[ $isRecursif -eq 0 ]]
            then
                titreInfo 'En mode non recursif les sous elements ne seront pas copiés.'
            fi

            titre3 'Copies des fichiers'
            local out='';
            local rep='';
            local filename='';
            #local -i index=0; # for test
            local -i _slashNb=0;
            local fichier_path;
            local fichier_source_relatif=''
            local fichier_path_destination=''

            for fichier_path in "${_donneesTriees[@]}"
            do
                if [[ $isTrapCAsk -eq 1 ]]
                then
                    erreursAddEcho "$FUNCNAME: Interuption par Ctrl C"
                    break;
                fi

                #if [ $index -gt 10 ];then break; fi # for debug

                fichier_path=$(echo $fichier_path | tr -d '\n'); # supprime les sauts de ligne
                if [ -z "$fichier_path" ];then continue; fi

                if [[ "${fichier_path:0:1}" == '\' ]];then fichier_path="${fichier_path:1}"; fi # suppression du slash de debut

                # - calcul de la source relative - #
                fichier_source_relatif="${fichier_path/$repertoire_source}" # suppression du repertoire source dans le chemin du fichier)
                fichier_source_relatif=${fichier_source_relatif#/}          # suppression du slash du debut (pour eviter le root)
                displayVarDebug 'fichier_source_relatif' "$fichier_source_relatif"

                # si pas recursif et qu'il y a un slash alors on passe au fichier suivant
                if [ $isRecursif -eq 0 ]
                then
                    _slashNb=$(echo "$fichier_source_relatif" | grep -o '/' | wc -l)
                    if [ $_slashNb -gt 0 ]; then echo -n '-'; continue; fi
                fi

                fichier_path_destination="$repertoire_destination$fichier_source_relatif"
                displayVarDebug 'fichier_path_destination' "$fichier_path_destination"

                if [ ! -f "$fichier_path_destination" ]
                then
                    echo -e "[$inodeFicPct%]cp \"$fichier_path_destination\"\c";
                    cp "$fichier_path" "$fichier_path_destination" 2>$cachePrefixe-cperror
                    local -i result=$?
                    if [ $result -eq 0 ]
                    then
                        echo $INFO'ok'$NORMAL
                        notifsAdd "[$inodeFicPct%]cp '$fichier_path_destination'";
                        ((inodeCompteur['fichiers_copies']++));
                        ((inodeFicLu++));inodeFicPct=$((inodeFicLu * 100 / inodeFicTotal))
                    else
                        echo $WARN'fail'$NORMAL
                        local error=$(cat $cachePrefixe-cperror)
                        ((inodeCompteur['fichiers_non_copies']++));
                        erreursAdd "[$inodeFicPct%]cp '$fichier_path_destination': $error";
                    fi
                else
                    # comparer les tailles et si differentes lancer la copie
                    # ...

                    #si taille egale
                    echo "[$inodeFicPct%]'$fichier_path_destination' existe deja."
                    ((inodeCompteur['fichiers_copies_existant']++))
                    ((inodeFicLu++));inodeFicPct=$((inodeFicLu * 100 / inodeFicTotal))
                fi
                #((index++)) # for debug

            done;

            if [ $isKeepCache -eq 0 ]
            then
                titre3 'Suppression des caches'
                if [ -f "$_cacheName" ];          then evalCmd "rm '$_cacheName'";fi
                if [ -f "$_cacheNameTri" ];  then evalCmd "rm '$_cacheNameTri'";fi
            fi
            
            ((actionNb++))

        fi #if [[ $isDironly -eq 0 ]]
        fctOut "$FUNCNAME"; return 0;
    } # copieFile()
#


###########
# compare #
###########
    function compareStats(){
        fctIn;
        (( actionNb++ ))
        stats_analyse "$repertoire_source"

        #affichier les stats de la destination uniquement en mode copie
        if [ $isCompareStats -eq 1 ];then stats_analyse "$repertoire_destination";fi
        fctOut "$FUNCNAME";return 0;
    }



    function compare(){
        fctIn "$*";
        compareInode '/home/pascal/.wget-hsts' ''
        fctOut "$FUNCNAME";return 0;
    }


    #compare 2 fichiers
    function compareInode(){
        fctIn "$*";
        if [[ $# -ne 2 ]]
        then
            titreWarn "Le nombre d'argument requis: $#/2"
            fctOut "$FUNCNAME"; return $E_ARG_REQUIRE;
        fi
        local _src="$1";    local _dest="$2";
        local -A _srcAttr;  local -A _destAttr;

        _srcAttr[0]='n/a'

        if [ ! -e "$_src" ]
        then
            _srcAttr['type']='n/a'
            #return
        fi
        if   [ -d "$_src" ];  then _srcAttr['type']='D'
        elif [ -L "$_src" ];  then _srcAttr['type']='L'
        elif [ -f "$_src" ];  then _srcAttr['type']='F'
        else _srcAttr['type']='O'
        fi

        #taille
        _srcAttr['sizeWC']=$(wc -c < "$_src")
        _srcAttr['sizeSTAT']=$(stat -c%s "$_src")

        # Rendu
        displayVar '_src' "$_src" 'type' "${_srcAttr['type']}" 'sizeWC' "${_srcAttr[sizeWC]}" 'sizeSTAT' "${_srcAttr[sizeSTAT]}"

        fctOut "$FUNCNAME";return 0;
    }
#

###########
# convert #
###########
    function convert(){
        fctIn "$*"

        if [ ! -d "$repertoire_source" ]
        then
            echo $WARN"$repertoire_source n'est pas un repertoire"$NORMAL
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi

        if [ ! -d "$repertoire_destination" ]
        then
            evalCmd "mkdir -p $repertoire_destination"
        fi

        # - Creation de la pile des fichiers (fullPath)- #
        local -A fichiers;
        local -i fichierNb=0;
        local srcPath="$repertoire_source*.$convertFrom"
        displayVar "source" "$repertoire_source ('$convertFrom' -> '$convertTo')"
        #displayVar "destPath" "$destPath"
        for fichier in $srcPath
        do
            # - calcul de la source relative - #
            fichier_source_relatif="${fichier/$repertoire_source}"
            fichiers[$fichierNb]="$fichier_source_relatif" 
            ((fichierNb++))
        done
        #displayVar "fichiers" "${fichiers[*]}"

        # - Trie de la pile - #
        IFS=$'\n';
        local sorted=($(sort <<<"${fichiers[@]}"))
        #local sorted=($(sort <<<"${fichiers[*]}"))
        unset IFS;unset fichiers
        #displayVar "sorted" "[$fichierNb]${sorted[*]}";

        # - Convertion des fichiers - #
        local filename=''
        local srcPath='';
        local destPath='';

        for fichier in ${sorted[@]}
        do
            if [[ $isTrapCAsk -eq 1 ]]
            then
                erreursAddEcho "$FUNCNAME: Interuption par Ctrl C"
                break;
            fi
            filename=${fichier%.*}
            srcPath="$repertoire_source$filename.$convertFrom"
            destPath="$repertoire_destination$filename.$convertTo"
            local notifTxt=""

            #echo ""
            #displayVar "fichier" "$fichier" "filename" "$filename"
            #displayVar "srcPath" "$srcPath" "destPath" "$destPath"
            notifTxt="Convertion de '$fichier': "
            echo -n "$notifTxt"
            
            if [ -f "$destPath" ]
            then
                echo "déjà convertit."
                notifsAdd "$notifTxt déjà convertit."
                ((inodeCompteur['convert_exist']++))
            else
                #warning error
                ffmpeg -loglevel error -i "$srcPath" "$destPath"
                if [ $? -ne 0 ]
                then
                    echo $WARN"$notifTxt FAIL"$NORMAL.
                    erreursAdd "$notifTxt FAIL."
                    ((inodeCompteur['convert_fail']++))
                else
                    echo $INFO" OK"$NORMAL.;
                    notifsAdd "$notifTxt OK."
                    ((inodeCompteur['convert_ok']++))
                fi
            fi
        done
        ((actionNb++))
        fctOut "$FUNCNAME";return 0;
    }
#


#########
# stats #
#########
    # resumé #
    function resumer(){
        fctIn "$*"
        if [[ $actionNb -eq 0 ]]
        then
            if [ "$repertoire_source" != "$repertoire_destination" ]
            then
                isCompareStats=1; #Le repertoire source et destination sont différentes donc fournis donc activation de la comparaison
            else
                displayDebug "Pas de résumé car aucune action n'a été effectué"
                fctOut "$FUNCNAME";return 0;
            fi
        fi
        isTrapCAsk=0; #reinitailiser le ctrlC
        titre1 'RÉSUMÉ'

        compareStats

        if [[ $isRename  -eq 1 ]]; then resumeRenommage; fi
        if [[ $isCopie   -eq 1 ]]; then resumeCopie;     fi
        if [[ $isConvert -eq 1 ]]; then resumeConvert;   fi

        if [[ $isKeepCache -eq 1 ]]
        then
            titre4 'Caches:'
            if [ -f "$cachePrefixe-dir.txt" ];then  ls -l $cachePrefixe-dir.txt;fi
            ls -l $cachePrefixe-file.txt
        fi
        fctOut "$FUNCNAME";return 0;
    }


    function resumeRenommage(){
        fctIn "$*"
        titre3   "RENAME\t lu \t modifié"
        echo -e   "REP   \t ${inodeCompteur['repertoires_lus']} \t ${inodeCompteur['repertoires_modifies']}";
        echo -e   "FIC   \t ${inodeCompteur['fichiers_lus']} \t ${inodeCompteur['fichiers_modifies']}";
        echo -e   "LIENS \t ${inodeCompteur['liens']}";
        echo ". Les modifications indiquent le nombre total des modifications. Un repertoire (ou fichier) peut etre modifié par plusieurs filtres."
        fctOut "$FUNCNAME";return 0;
    }


    function resumeCopie(){
        fctIn "$*"
        titre3 "COPIE   % \t ok \t exist \t fail"
        echo -e   "REP   $inodeRepPct \t ${inodeCompteur['repertoires_copies']} \t  ${inodeCompteur['repertoires_copies_existant']} \t ${inodeCompteur['repertoires_non_copies']}";
        echo -e   "FIC   $inodeFicPct \t ${inodeCompteur['fichiers_copies']} \t  ${inodeCompteur['fichiers_copies_existant']} \t ${inodeCompteur['fichiers_non_copies']}";
        fctOut "$FUNCNAME";return 0;
    }


    function resumeConvert(){
        fctIn "$*"
        titre3 "\nCONVERT  ok \t exist \t fail "
        echo -e   "FIC     ${inodeCompteur['convert_ok']} \t ${inodeCompteur['convert_exist']} \t  ${inodeCompteur['convert_fail']}";
        fctOut "$FUNCNAME";return 0;
    }


    #stats ($repertoire)
    declare -A stats_analyseResult;
    function stats_analyse(){
        fctIn "$*"

        if [ $actionNb -eq 0 -a $isStats -eq 0 ];then fctOut "$FUNCNAME"; return $E_F1; fi
        local repertoire="$1";

        # on determine si c'est la source ou la destination
        local _srcDest='src'
        if [[ "$repertoire" == "$repertoire_destination" ]]; then _srcDest='Dest'; fi
        # on supprime le slashe de fin si existe
        repertoire=${repertoire%/}

        titreStats "Analyse du répertoire:'$repertoire' (copie du resultat dans $cachePrefixe-$_srcDest-stats.txt)"

        # initialisation de la variable global de resultat
        stats_analyseResult['inodeTotal']=0;
        stats_analyseResult['repNb']=0;
        stats_analyseResult['fileNb']=0;
        stats_analyseResult['linkNb']=0;
        stats_analyseResult['otherNb']=0;
        stats_analyseResult['sizeTotal']=0;

        echo '' > $cachePrefixe-stats.txt

        if [[ ! $# -eq 1 ]]
        then
            titreUsage 'Usage stats_analyseRepertoire(repertoire).'
            fctOut "$FUNCNAME";return $E_F1;
        fi

        if [[ ! -d "$repertoire" ]]
        then
            titreWarn "'$repertoire' n'est pas un repertoire."
            fctOut "$FUNCNAME";return $E_INODE_NOT_EXIST;
        fi
        stats_analyseRepertoire "$repertoire"

        # afficher le resultat
        local _result=Total=${stats_analyseResult['inodeTotal']}" dont: repertoires="${stats_analyseResult['repNb']}" fichiers="${stats_analyseResult['fileNb']}" Liens="${stats_analyseResult['linkNb']}" Autres="${stats_analyseResult['otherNb']} #sizeTotal="${stats_analyseResult['sizeTotal']}
        titreInfo "$_result"
        echo "$_result" >> "$cachePrefixe-stats.txt";
        fctOut "$FUNCNAME";return 0;
    }


    # fait un decompte des types d'inode dans le rep fournis
    function stats_analyseRepertoire(){
        #fctIn "$*" # fonction recursive

        local repertoire="$1";
        for inode in $repertoire/*
        do
            if [[ $isTrapCAsk -eq 1 ]]
            then
                erreursAddEcho "$FUNCNAME: Interuption par Ctrl C"
                break;
            fi

            ((stats_analyseResult['inodeTotal']++))
            displayDebug "inode(${stats_analyseResult['inodeTotal']}): $inode" 3

            if [[ -d "$inode" ]]
            then
                ((stats_analyseResult['repNb']++));
                if [ $isRecursif -eq 1 ]
                then
                    #echo "DIR_: $inode" >> $cachePrefixe-stats.txt
                    stats_analyseRepertoire "$inode"
                fi
            elif [[ -L "$inode" ]]
            then
                ((stats_analyseResult['linkNb']++));
                #echo $INFO"LINK: $inode"$NORMAL
                echo "LINK: $inode" >> $cachePrefixe-stats.txt
            elif [[ -f "$inode" ]]
            then
                ((stats_analyseResult['fileNb']++));
            else
                ((stats_analyseResult['otherNb']++));
                #echo $INFO"OTHE: $inode"$NORMAL
                echo "OTHE: $inode" >> $cachePrefixe-stats.txt
            fi


        #local -i tailleFichier=$(stat -c "%s" "$inode")
        #echo "taille fichier stat="$tailleFichier
        #tailleFichier=$(wc -c <  "$inode")
        #echo "taille fichier wc="$tailleFichier
        #tailleFichier=$(du -b "$inode")
        #echo "taille fichier du="$tailleFichier
            
        #stats_analyseResult['sizeTotal']=$((${stats_analyseResult['sizeTotal']}+$tailleFichier));
        done;
        #fctOut "$FUNCNAME"; # fonction recursive
        return 0;
    }
#

# ########## #
# PARAMETRES #
# ########## #
TEMP=$(getopt \
     --options v::dVhnpRc:: \
     --long help,version,verbose::,debug,showVars,showCollections,update,conf::,showLibs,showDuree\
,profondeur_max::,dironly,fileonly,keepCache,fromCache,cachePrefixe::,fichierMotif::,compareStats\
,rename,copy,compare,convert,convertFrom::,convertTo::,stats,createCache\
     -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"
        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                shift ;;
            -V|--version) echo "$VERSION"; exit 0;      shift ;;
            -v|--verbose)
                case "$parametre" in
                    '') verbosity=1; shift 2 ;;
                    *)  #echo "Option c, argument '$2'" ;
                    verbosity=$parametre; shift 2;;
                esac
                ;;
            -d|--debug) ((isDebug++));isShowVars=1;         shift  ;;
            --update)   isUpdate=1;                     shift ;;

            --showVars)         isShowVars=1;           shift  ;;
            --showCollections)  isShowCollections=1;    shift  ;;
            --showLibs )        isShowLibs=1;           shift  ;;
            --showDuree )       isShowDuree=1;          shift  ;;

            --conf)     CONF_PSP_PATH="$parametre";  shift 2; ;;

            -S)
                isSimulation=1;
                YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
                shift
                ;;

            --collection)       collection=$parametre; shift 2 ;;


            # - parametres liés au projet - #
            #--createTest) isCreateTest=1; createTest; shift; ;;
            #--test)       isTest=1;       lanceTest;  shift; ;;
            --rename)     isRename=1;     shift; ;;
            --keepCache)  isKeepCache=1;  shift; ;;

            --createCache)
                isCreateCache=1;
                isKeepCache=1; # force la preservation du cache
                shift;
                ;;

            --cachePrefixe) cachePrefixe="$2";  shift 2; ;;
            --fromCache)    isFromCache=1;      shift;   ;;
            -c|--copy)      isCopie=1;isCompareStats=1;  shift;   ;;
            --compare)      isCompare=1;        shift;   ;;
            --convert)      isConvert=1;        shift;   ;;
            --convertFrom)  convertFrom="$2";   shift 2; ;;
            --convertTo)    convertTo="$2";     shift 2; ;;

            -R)             isRecursif=1;       shift;   ;;
            --dironly)      isDironly=1;        isRecursif=1;      shift;   ;;
            --fileonly)     isFileonly=1;       shift;   ;;
            --stats)        isStats=1;          shift;   ;;
            --compareStats) isCompareStats=1;   shift;   ;;

            -p|--profondeur_max )
                # on accepte la valeur demandé
                PROFONDEUR_MAX=$2;

                # on limite les valeurs si elle depasse
                if [ $2 -lt 0 ]
                then
                    PROFONDEUR_MAX=0;
                    titreWarn "PROFONDEUR_MAX=$PROFONDEUR_MAX"
                fi
                if [ $2 -gt 9 ]
                then
                    PROFONDEUR_MAX=9;
                    titreWarn "PROFONDEUR_MAX=$PROFONDEUR_MAX"
                fi
                shift 2
                ;;

    
            --fichierMotif)
                #fichierMotif="\"$2\"";
                fichierMotif="$2"
                shift 2
                ;;

            # - Les parametres supplementaires - #
            --)
                repertoire_source="${2:-"./"}"
                repertoire_destination=${3:-"./"}
                shift;
                break
                ;;
            
            *)
                repertoire_source="${1:-"./"}"
                repertoire_destination=${2:-"./"}
                shift;
                break
        esac
    done
#


########
# main #
########

beginStandard


if [[ "$repertoire_source" == '' ]]
then
    erreursAddEcho 'Une source doit etre defini.'
    endStandard
    exit $E_ARG_BAD;
fi


if [[ ! -d "$repertoire_source" ]]
then
    titreWarn "Le repertoire source '$repertoire_source' n'est pas un repertoire!"
    endStandard
    exit $E_ARG_BAD;
else
    displayDebug "'$repertoire_source' est bien un repertoire." $LINENO;
fi


repertoire_source=${repertoire_source%/}
repertoire_source="$repertoire_source/"
repertoire_destination=${repertoire_destination%/}
repertoire_destination="$repertoire_destination/"

displayVar "repertoire_source" "$repertoire_source"
displayVar "repertoire_destination" "$repertoire_destination"


if [[ $isCreateCache -eq 1 ]]
then
    createCache 'd'
    createCache 'f'
fi

if [[ $isRename -eq 1 ]]
then
    titre1 'RENAME'
    renameRep "$repertoire_source"
fi

if [[ $isCopie -eq 1 ]]
then
    copier
fi

if [[ $isCompare -eq 1 ]]
then
    compare
fi

if [[ $isConvert -eq 1 ]]
then
    convert
fi

if [ $isKeepCache -eq 0 -a -f "$cachePrefixe-dir.txt" ]
then
    rm "$cachePrefixe-dir.txt";
fi

if [[ $isStats -eq 1 ]]
then
    evalCmd "stats_analyse '$repertoire_source'"
fi


endStandard

resumer # apres la sortie standard

#echo "FIN NORMAL"
if [[ $isTrapCAsk -eq 1 ]];then exit $E_CTRL_C;fi
exit 0 # https://abs.traduc.org/abs-fr/apd.html#exitcodesref

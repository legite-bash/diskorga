#!/bin/bash
declare -r -i SCRIPT_DEBUT=$(date +%s);
#declare -r SCRIPT_PATH=$(realpath $0);         # chemin  absolu complet du script rep + nom
#declare -r SCRIPT_REP=$(dirname $SCRIPT_PATH); # repertoire absolu du script (pas de slash de fin)
declare -r SCRIPT_FILENAME="${0##*/}";            # /path/nom.ext.ext -> nom.ext
declare -r SCRIPT_NAME="${SCRIPT_FILENAME%.*}";    # nom.pasext.ext ->             # uniquement le nom
declare -r SCRIPT_EXT="${SCRIPT_FILENAME##*.}";    # nom.pasext.ext -> ext          uniquement l'extention
declare -r LEGITELIB_VERSION='2022.07.22-PSP';
#declare -r LOG_PATH="/tmp/$SCRIPT_FILENAME-$SCRIPT_DEBUT.txt"


declare -ri IS_LEGITELIB_CALL=1;            # indique que cette lib est chargé

if [[ -w /dev/shm ]]
then
    declare LOG_ROOT='/dev/shm';
    declare TMP_REP='/dev/shm';
else
    declare LOG_ROOT='/tmp';
    declare TMP_REP='/tmp';
fi
declare TEST_REP="$TMP_REP";


#######
# LOG #
#######
declare LOG_PATH="$LOG_ROOT/$SCRIPT_NAME.txt";

function setLogPath(){
    local _logPath="${1:-""}";
    if [[ "$_logPath"  == '' ]];then return $E_ARG_BAD;fi

    LOG_PATH="$LOG_ROOT/$_logPath.log";
    return 0;
}



################
# COLORISATION #
################
    declare -r RESET_COLOR=$(tput sgr0);
    declare -r BOLD=$(tput smso);
    declare -r NOBOLD=$(tput rmso);

    declare -r BLACK=$(tput setaf 0);
    declare -r RED=$(tput setaf 1);
    declare -r GREEN=$(tput setaf 2);
    declare -r YELLOW=$(tput setaf 3);
    declare -r CYAN=$(tput setaf 4);
    declare -r MAGENTA=$(tput setaf 5);
    declare -r BLUE=$(tput setaf 6);
    declare -r WHITE=$(tput setaf 7);

    #declare -r   BLACK="\[\033[01;30m\]"
    #declare -r     RED="\[\033[01;31m\]"
    #declare -r   GREEN="\[\033[01;32m\]"
    #declare -r  YELLOW="\[\033[01;33m\]"
    #declare -r    CYAN="\[\033[01;34m\]"
    #declare -r MAGENTA="\[\033[01;35m\]"
    #declare -r    BLUE="\[\033[01;36m\]"
    #declare -r   WHITE="\[\033[01;37m\]"

    declare    NORMAL=$WHITE;
    declare    INFO=$BLUE;
    declare    CMD=$YELLOW;
    declare    WARN=$RED;
    declare    TITRE1=$GREEN;
    declare    TITRE2=$MAGENTA;
    declare    TITRE3=$CYAN;
    declare    TITRE4=$CYAN;
    declare    DEBUG_COLOR=$MAGENTA;
    declare    FUNCTION_COLOR=$CYAN;
#


##########################
# COLORISATION:TITRE,ETC #
##########################
    function titre1() {
        if [ $# -eq 0 ];then return 0;fi
        local -i _carNb=$(( ${#1} + 4 ))
        local C='#';
        echo '';
        dupliqueNCar $_carNb "$C";
        echo -e "$C$TITRE1 ${1} $NORMAL$C";
        dupliqueNCar $_carNb "$C";
    }

    function Titre1() {
        if [ $# -eq 0 ];then return 0;fi
        titre1 "${1^}";
    }

    function TITRE1() {
        if [ $# -eq 0 ];then return 0;fi
        titre1 "${1^^}";
    }

    function titre2() {
        if [ $# -eq 0 ];then return 0;fi
        local -i _carNb=$(( ${#1} + 0 ))
        local C='='
        local _sep=$(dupliqueNCar $_carNb "$C")
        echo ''
        echo "  $_sep"
        echo -e $TITRE2"  ${1}"$NORMAL
    }

    function titre3() {
        if [ $# -eq 0 ];then return 0;fi
        echo '';
        echo -e $TITRE3"    ###  ${1}  ###"$NORMAL
    }

    function titre4() {
        if [ $# -eq 0 ];then return 0;fi
        echo '';
        echo -e $TITRE4"      ====  ${1}  ===="$NORMAL
    }

    function titreUsage() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $MAGENTA"=============="
        echo "$1"$NORMAL
    }

    function titreCmd() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $CMD"$1"$NORMAL
    }

    function titreInfo() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $INFO"$1"$NORMAL
    }

    function titreWarn() {
        if [ $# -eq 0 ];then return 0;fi
        echo -e $WARN"$1"$NORMAL
    }

    function titreLib() {
        if [ $# -eq 0 ];then return 0;fi
        echo '';
        echo -e "${INFO}=== LIBRAIRIE: $1 ==="$NORMAL
    }
#


####################
# CODES DE SORTIES #
####################

    # https://man7.org/linux/man-pages/man7/signal.7.html
    # http://www.gnu.org/software/bash/manual/html_node/Exit-Status.html#Exit-Status
    # https://abs.traduc.org/abs-5.0-fr/apd.html#exitcodesref
    # https://tldp.org/LDP/abs/html/exitcodes.html

    declare    codeRetourLast='';         # complement d'information venant de la derniere erreur
    declare -a codeRetoursDesc;

    codeRetoursDesc[0]='OK';

    declare -r -i E_MISC=1;             codeRetoursDesc[1]='Erreur générale ou opération interdite ou FAUX logique';

    # 64..113: USER EXIT (compatible C)
    declare -r -i E_NORMAL=64;          codeRetoursDesc[$E_NORMAL]='E_NORMAL: Mais qu est ce donc une erreur normale?';
    declare -r -i E_ARG_NONE=65;        codeRetoursDesc[$E_ARG_BAD]='E_ARG_BAD: Mauvais argument';
    declare -r -i E_ARG_BAD=66;
    codeRetoursDesc[$E_ARG_NONE]='E_ARG_NONE (obsolete)';
    declare -r -i E_ARG_REQUIRE=67;     codeRetoursDesc[$E_ARG_REQUIRE]='E_ARG_REQUIRE: Argument requis non présent';

    declare -r -i E_INTERNE=70;         codeRetoursDesc[$E_INTERNE]='Erreur interne (fonction vide, pb de permission,etc)';

    declare -r -i E_TRUE=0;             codeRetoursDesc[$E_TRUE]='E_TRUE:Vrai (vaut 0)';
    declare -r -i E_FALSE=97;           codeRetoursDesc[$E_FALSE]=':E_FALSE:Faux';
    declare -r -i E_NOT_ROOT=98;        codeRetoursDesc[$E_NOT_ROOT]='E_NOT_ROOT: Neccessite un droit root.';
    declare -r -i E_UPDATE=99;          codeRetoursDesc[$E_UPDATE]='E_UPDATE: indique que la mise à jours a été faite.';

    #100..113 (sortie de fonction)
    declare -r -i E_F0=100;             codeRetoursDesc[$E_F0]="E_F0:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F1=101;             codeRetoursDesc[$E_F1]="E_F1:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F2=102;             codeRetoursDesc[$E_F2]="E_F2:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F3=103;             codeRetoursDesc[$E_F3]="E_F3:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F4=104;             codeRetoursDesc[$E_F4]="E_F4:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F5=105;             codeRetoursDesc[$E_F5]="E_F5:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F6=106;             codeRetoursDesc[$E_F6]="E_F6:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F7=107;             codeRetoursDesc[$E_F7]="E_F7:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F8=108;             codeRetoursDesc[$E_F8]="E_F8:Sortie d'une fonction avec l'erreur personalisée 0";
    declare -r -i E_F9=109;             codeRetoursDesc[$E_F9]="E_F9:Sortie d'une fonction avec l'erreur personalisée 0";

    declare -r -i E_INODE_NOT_EXIST=110;codeRetoursDesc[$E_INODE_NOT_EXIST]="E_INODE_NOT_EXIST: Erreur lors de l'accès à un fichier ou à un répertoire.";
    declare -r -i E_INODE_NOT_R=111;    codeRetoursDesc[$E_INODE_NOT_R]="FICHIER/DIRECTORY NOT READABLE";
    declare -r -i E_INODE_NOT_W=112;    codeRetoursDesc[$E_INODE_NOT_W]="FICHIER/DIRECTORY NOT WRITABLE";

    #>=129.255 FATAL_ERROR
    declare -r -i E_CMD_CANT_EXEC=126;  codeRetoursDesc[$E_CMD_CANT_EXEC]='E_CMD_CANT_EXEC';
    declare -r -i E_CMD_NOT_FOUND=127;  codeRetoursDesc[$E_CMD_NOT_FOUND]='E_CMD_NOT_FOUND';

    declare -r -i E_CTRL_C=130;         codeRetoursDesc[$E_CTRL_C]='E_CTRL_C';


    declare -i codeRetour=0;
    #fonction setCodeRetour( erreurNo  descriptionComplementaire)
    function setCodeRetour() {
        codeRetour=${1:-0};
        codeRetourLast=${2:-''};
    }

    function displayCodeRetour(){

        # si pas d'argument on utilise $?
        if [[ $# -eq 0 ]]
        then
            displayVar $? "${codeRetoursDesc["$?"]}";
            return 0;
        fi

        local -i _erreurNo=$1;
        local _txt="${codeRetoursDesc["$_erreurNo"]:-'Pas de description.'}";
        displayVar $_erreurNo "$_txt";
        return 0;
        }

#


# ##### #
# TRAPS #
# ##### #
    #http://www.gnu.org/software/bash/manual/html_node/Bourne-Shell-Builtins.html#Bourne-Shell-Builtins
    #trap -l #trap --help
    #https://linuxhint.com/bash_error_handling/
    #https://tldp.org/LDP/Bash-Beginners-Guide/html/sect_12_02.html
    #trap 'e=$?; echo "Erreur no $e. Ligne $LINENO: $ {codeRetoursDesc[$e]}.$codeRetourLast";' ERR


    # initialise  les interceptions
    # Les traps doivent etre initialisées apres la fonction shift car elle renvoie 1 et provoque donc une exeption générale
    function trapInit(){
        fctIn;
        trap 'trapErrAdd $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}";'                        ERR;
        trap 'trapErrAdd $? $LINENO "${BASH_SOURCE}" "${FUNCNAME[*]}" "TRAP C";isTrapCAsk=1;'  SIGINT;
        fctOut "$FUNCNAME(0)";return 0;
    }
    
    function ctrl_c() {
        fctIn;
        isTrapCAsk=1;
        fctOut "$FUNCNAME(0)";return 0;
    }

    # - TRAP: INTERCEPTION- #
    #trap 'echo "$WARN"catch: ligne=$LINENO code_retour=$?"$NORMAL"' ERR
    #trap 'echo $DEBUG_COLOR"ligne=$LINENO code_retour=$?"$NORMAL' DEBUG

    # - TRAP: PILE - #
    declare -a trapErrPile;              # tableau contenant la pile de notifications
    declare -i trapErrPileIndex=0;       # index utilisé pour le remplissage de la pile des erreur

    #trapErrAdd($txt) # ajoute une interception dans la pile
    function trapErrAdd() {
       fctIn "$*";

        local -i _errNu=$1;
        #if [[ $_errNu -eq 0 ]];then return 0;fi
        local -i _ligneNo="$2";
        local    _src="${3:-''}";
        local    _fonction="${4:-''}";
        local    _funcName="${5:-''};";
        local    _description=${codeRetoursDesc[$_errNu]:-'aucune'}
        local out="Erreur no $_errNu. $_src:$_ligneNo. $_funcName Description: $_description. codeRetourLast: $codeRetourLast";
        displayVarDebug 'TRAP' "${WARN}$out$NORMAL";

        trapErrPile[$trapErrPileIndex]="$out"
        ((trapErrPileIndex++))
        fctOut "$FUNCNAME(0)";return 0;
    }

    #trapErrShow($separateur="\n") # affiche la pile
    function trapErrShow() {
        #if [[ -z ${trapErrPile[*]} ]]; then return 255; fi
        fctIn "$*";
        local -i _trapNb=0
        if [[ ! -z "${trapErrPile[*]}" ]];then _trapNb=${#trapErrPile[*]}; fi
        if [[ $_trapNb -ne 0 ]]
        then
            echo -e "\n"$TITRE1"TRAPS($_trapNb)"$NORMAL
            local separateur=${1:-"\n"}
            for valeur in "${trapErrPile[@]}"
            do
                echo -ne "  $valeur$separateur";
            done
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }
#


#############
# VARIABLES #
#  GLOBALES #
#############

    # --- variables systemes --- #
    # http://redsymbol.net/articles/unofficial-bash-strict-mode/
    declare -r IFS_DEFAULT=$'\n\t';
    IFS="$IFS_DEFAULT" # http://redsymbol.net/articles/unofficial-bash-strict-mode/

    # --- variables des includes --- #
    declare -r UPDATE_HTTP_ROOT="http://updates.legite.org";
    declare -r UPDATE_HTTP="$UPDATE_HTTP_ROOT/$SCRIPT_NAME";

    declare CONF_REP=${CONF_REP:-"$HOME/.legite/$SCRIPT_NAME"};

    declare CONF_ORI_PATH="$CONF_REP/$SCRIPT_NAME.ori.sh";
    declare CONF_SRC_PATH="$SCRIPT_REP/$SCRIPT_NAME.ori.sh";
    #declare CONF_FILENAME="$SCRIPT_NAME.cfg.sh";
    declare CONF_USR_PATH="$CONF_REP/$SCRIPT_NAME.cfg.sh";
    declare CONF_PSP_PATH="";           # fournis en parametre

    declare COLLECTIONS_REP="$CONF_REP/collections";
    declare -i isCollectionExecAuto=0;  # setIntegral(): la colection doit elle s'excuter entierement? (utilsé par dl.sh)
    declare -i isCollectionFail=0;      # indique qu'il y  un problème en amont dans la collection (desactive le lancement des libs de la collection actuelle)

    # --- Definir si root ou user --- #
    [[ "$(id -u)" == "0" ]] && declare -r -i IS_ROOT=1 || declare -r -i IS_ROOT=0;

    # --- tag d'interuption --- #
    declare -i isTrapCAsk=0;        # demande d'interuption par control C

    # --- variables generiques --- #
    declare -r -i argNb=$#;
    declare argument='';            # argument actuel ($1)
    declare parametre='';           # parametre actuel ($2)
    declare -i isUpdate=0;          # demande de update
    declare -i verbosity=0;         # niveau de verbosité defini via la ligne de cmd
    declare -i vic=0;               # Verbode In Code: n niveau par defaut de verbose pour echoV
    declare -i isDebug=0;
    declare -i isShowVars=0;        # montrer les variables
    declare -i isShowLibs=0;        # 1:vue trier des index. 2: vue triés index+description

    declare -i isShowDuree=0;       # Affiche la duree du script
    declare -i evalErr=0;           # retour de la cmd evalCmd
    declare -i isSimulation=0       # 

    declare -i isLibExecAuto=1;     # Par défaut la librairie homonyme à la collection est automatiquement chargé


    # - PSPCall - #
    declare -A PSPCall;             # (array) liste des PSP(datas) non PSPParamsl (collections/librairies/autres) appellées pour etre executées
    PSPCall[0]='';                  # meta info ne contient pas de nom de PSP (reservé pour un usage futur)
    declare -i PSPCallNb=0;         # nb(-1) de PSP dans le tableau PSPCall
    declare -i PSPCallIndex=0;      # index courant du tableau PSPCall
    #declare -A librairiesCall;         # (array) liste des librairies appellées pour etre executées (OBSOLETE)
    #declare -i librairiesCallIndex=0;  # index pour construire le tableau

    # - collections - #
    declare -i isShowCollections=0; #
    declare -i isListeCollections=0;# 
    declare    collectionNom='';    # nom de la collection actuelle (loadCollection)
    declare -A collectionExecTbl;   # tableau: La collection a t'elle déj) été executée?


    # - librairies - #
    declare    librairie='';        # librairie en cours
    declare -A librairiesTbl;       # table des librairies-fonctions qui sont enregistrées (et pouvant etre executées)
    declare -A librairiesDescr;     # description de chaque librairie
    declare -A libExecTbl;          # tableau: La librairie a t'elle déj) été executée?


    # - PSPParams - #
    declare -i isPSPParams=0;       # les parametres en cours sont t'ils des parametres pour les librairies?
    declare -i isPSPSup=0;          # obsolete (new: isPSPParams)
    declare -A PSPParams;           # tableau associatifs contenant les parametre supplémentaire
    declare -A libParams;           # abosolete (PSPParams)
#


#################
# NOTIFICATIONS #
#################
    declare -a notifsPile;          # tableau contenant la pile de notifications
    declare -i notifsIndex=0;       # index utilisé pour le remplissage de la pile de notifs

    #notifsAdd($txt) # ajoute une notif dans la pile de notifs
    function notifsAdd() {
        local _notif=${1:-""}
        if [[ "$_notif"  == '' ]];then  return 0;   fi

        notifsPile[$notifsIndex]="$_notif"
        ((notifsIndex++))
        return 0;
    }

    function notifsAddEcho() {
        local _notif=${1:-""}
        if [[ "$_notif"  == '' ]];then  return 0;   fi

        notifsPile[$notifsIndex]="$_notif"
        echo "$_notif" $INFO'ok'$NORMAL
        ((notifsIndex++))
        return 0;
    }

    function notifsAddEchoV() {
        if [ $# -lt 2 ];then return $E_ARG_REQUIRE;fi
        local -i _verbosity=$1
        #displayVar '_verbosity' "$_verbosity" 'verbosity' "$verbosity"
        if [ $_verbosity -gt $verbosity ];then return 0;fi 
        local _notif=${2:-""}
        notifsAddEcho "$_notif"
        return 0;
    }

    #notifsShow($separateur="\n") # affiche la pile des notifs
    function notifsShow() {
        fctIn "$*";
        if [[ -z "${notifsPile[*]}" ]];then fctOut "$FUNCNAME(0)";return 0;fi
        local _separateur=${1:-"\n"}
        titre1 "Notifs($notifsIndex)"
        for _notif in "${notifsPile[@]}"
        do
            echo -ne "$_notif$_separateur";
        done
        fctOut "$FUNCNAME(0)";return 0;
    }
#


###########
# ERREURS #
###########
    declare -a erreursPile;              # tableau contenant la pile de notifications
    declare -i erreursPileIndex=0;       # index utilisé pour le remplissage de la pile des erreurs

    #erreursAdd($txt) # ajoute une notif dans la pile de notifs
    function erreursAdd() {
        local _erreur=${1:-''}
        if [[ "$_erreur"  == '' ]];then  return $E_ARG_REQUIRE;   fi
        erreursPile[$erreursPileIndex]="$_erreur"
        ((erreursPileIndex++))
        return 0;
    }

    function erreursAddEcho() {
        local _erreur=${1:-''}
        if [[ "$_erreur"  == '' ]];then  return $E_ARG_REQUIRE;   fi

        erreursPile[$erreursPileIndex]="$_erreur"
        echo $WARN"$_erreur"$NORMAL
        ((erreursPileIndex++))
        return 0;
    }

    function erreursAddEchoV() {
        if [ $# -lt 2 ];then return $E_ARG_REQUIRE;fi
        local -i _verbosity=$1
        #displayVar '_verbosity' "$_verbosity" 'verbosity' "$verbosity"
        if [ $_verbosity -gt $verbosity ];then return 0;fi 
        local _erreur=${2:-""}
        erreursAddEcho "$_erreur"
        return 0;
    }

    #erreursShow($separateur="\n") # affiche la pile des erreurs
    function erreursShow() {
        fctIn "$*";
        if [[ -z "${erreursPile[*]}" ]];then fctOut "$FUNCNAME(0)";return 0;fi
        local _separateur=${1:-"\n"}
        titre1 "Erreurs($erreursPileIndex)"
        for _erreur in "${erreursPile[@]}"
        do
            echo -ne "$_erreur$_separateur";
        done
        fctOut "$FUNCNAME(0)";return 0;
    }

#


#########
# DEBUG #
#########
    ## -- NIVEAU DE DEBUG -- ##
    #-d = -1d
    # * affichage du nom de la fonction en entrant
    #-2d
    # * affichage du nom de la fonction en sortant


    ## -- CMD -- ##
        #evalCmd('cmd' [ligneNu] [txt])
        function evalCmd() {
            #fctIn "$*";
            if [[ $# -gt 0 ]]
            then
                local ligneNu=${2:-''}
                if [[ $isDebug -eq 0 ]];then ligneNu='';fi #afficher le numero de ligne que pour le mode debug
                local txt=${3:-''}

                echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
                eval "$1";
                evalErr=$?          #global
                return $evalErr
            fi
            #fctOut "$FUNCNAME(0)";return 0;
        }

        #evalCmdDebug('cmd' ligneNu 'txt' debugLevel )
        # execute et affiche la commande si debug positif
        function evalCmdDebug() {
            fctIn "$*";
            if [[ $# -gt 1 ]]
            then
                local ligneNu=${2:-''}
                local txt=${3:-''}
                local debugLevel=${4:-1}

                echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";
                eval "$1";
                evalErr=$?
                fctOut "$FUNCNAME";return $evalErr     #global
            fi
            fctOut "$FUNCNAME(0)";return 0;
        }

        #evalCmdDebug('cmd' ligneNu 'txt' debugLevel )
        # execute toujours la commande mais fait un echo affiche la commande si debug positif
        function evalCmdEchoDebug() {
                fctIn "$*"
                local ligneNu=${2:-''}
                local txt=${3:-''}
                local debugLevel=${4:-1}

                if [ $isDebug -gt 0  ]; then echo "${DEBUG_COLOR}$ligneNu$CMD$1$NORMAL$txt";fi
                eval "$1";
                evalErr=$?          #global
                fctOut "$FUNCNAME($evalErr)";return $evalErr;
        }
    #

    ## -- AFFICHAGE -- ##
        #displayDebug( 'txt' verbosité 'pre' 'post')
        # verbosité: Afficher le texte quand _lvl inf ou egal à isDebug
        function displayDebug(){
            local    _txt="${1:-''}";
            local -i _lvl=${2:-1};
            if [[ $isDebug -eq 0 ]];then return 0;fi
            if [[ $_lvl    -le $isDebug ]]
            then
                local _pre=${3:-''}
                local _post=${4:-''}

                echo -e ${DEBUG_COLOR}"$_pre$_txt$_post"$NORMAL
            fi
            return 0;
        }

        #alias pour la retrocompatibilité car displayDebug est obsolete
        #function displayDebug() {
        #    displayDebug "$@"
        #}

        #displayVar "nom" "val" 
        function displayVarNotNull() {
            if [[ $# -ne 2 ]];then return $E_ARG_REQUIRE; fi
            if [[ "$2" != '' ]]
            then
                displayVar "$1" "$2";
            fi
            return 0;
        }

        #displayVar "nom" "val" 
        function displayVar() {
            local out='';
            while [[ $# -gt 1 ]]
            do
                out="$out$INFO$1:$NORMAL'$2'"
                shift 2;
            done
            #echo "$tabulationsFct$out";
            echo "$out";
            return 0;
        }


        function displayVarDebug() {
            if [[ $isDebug -gt 0 && $# -gt 1 ]]
            then
                displayVar "$@";
            fi
            return 0;
        }
    #
        #Affiche la durée d'execution du script
        function showDuree() {
            fctIn "$*";
            if [[ $isShowDuree -eq 1 ]]
            then
                local -r -i SCRIPT_FIN=$(date +%s);
                local -r -i SCRIPT_DUREE=$(($SCRIPT_FIN-$SCRIPT_DEBUT));
                echo "$SCRIPT_DUREE secondes";
            fi
            fctOut "$FUNCNAME(0)";return 0; 
        }
#


################
# MISE A JOURS #
################
    # telecharge un fichier et archive la version local
    # updateFile ("src" "dest") # chemin absolu
    # E_F1: en cas d'echec
    function updateFile() {
       fctIn "$*"
        if [[ $# -ne 2 ]]
        then
            titreWarn "Le nombre d'argument requis est: $#/2"
            fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        local src="$1"; local dest="$2";
        titre3 "Update de '$src'"

        local _dbak=$(date +'%m-%d-%HH%Mm');
        evalCmd "cp '$dest' '$dest-$_dbak.bak'";     # Fait une sauvegarde
        evalCmd "rm '$dest'";

        evalCmd "wget -O '$dest' '$src'"
        evalCmd "chmod +x '$dest'"
        if [ $? -ne 0 ]
        then
            erreursAddEcho "Erreur lors de l'installation de $dest."
            evalCmd "mv '$dest.bak' '$dest'"
            fctOut "$FUNCNAME($E_F1)"; return $E_F1;
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }

    # - telecharge et installe la derniere version dans le repertoire du script en remplacant le script qui a lancer l'update - #
    function selfUpdate () {
        fctIn
        if [ $isUpdate -eq 1 ]
        then
            echo "mise a jours du programme";
            if [ ! -d "$CONF_USR_ROOT"   ];then mkdir -p "$CONF_USR_ROOT";fi
            if [ ! -d "$COLLECTIONS_REP" ];then mkdir -p "$COLLECTIONS_REP";fi

            # - update de la libraire - #
            # - update du script - #
            updateFile "$UPDATE_HTTP/$SCRIPT_FILENAME" "$SCRIPT_REP/$SCRIPT_FILENAME"

            # - update des fichiers de configurations - #
            if [ ! -d "$CONF_USR_ROOT" ];then mkdir "$CONF_USR_ROOT";fi
            updateFile "$UPDATE_HTTP/CONF_ORI_FILENAME"  "$CONF_ORI_PATH"
            updateFile "$UPDATE_HTTP/CONF_USR_FILENAME"  "$CONF_USR_PATH"

            # - update des collections - #
            #dans selfUpdateLocal

            selfUpdateLocal
            exit $E_UPDATE; # sortie normal avec indicateur E_UPDATE
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }


    # a surcharger par le projet
    function selfUpdateLocal() {
        fctIn
        if [ $isUpdate -eq 1 ]
        then
            titre3 "mise a jours specifique au programme";

            if [ $IS_ROOT -eq 1  ]
            then
                displayDebug ''
            fi
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }
#


# ############## #
# EXEC / REQUIRE #
# ############## #
    # creer et execute les fichiers locaux
    # execFile(localName){
    function execFile() {
        fctIn "$*"
        local localName=${1:-''};
        if [[ -z "$localName" ]]; then  fctOut "$FUNCNAME($E_ARG_BAD)"; return $E_ARG_BAD; fi
        if [[ -f "$localName" ]]
        then
            if [[ ! -x "$localName" ]];then evalCmd "chmod +x $localName;"; fi
            #echo "nano $localName # Pour modifier le fichier"
            #evalCmd ". $localName";
            source "$localName";
            local -i _lastErrNu=$?;
            fctOut "$FUNCNAME($_lastErrNu)"; return $_lastErrNu;
        fi
        if [[ $verbosity -gt 0 ]];then titreWarn "'$localName' n'existe pas.";fi
        fctOut "$FUNCNAME($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
    }


    # Charge un fichier et met fin au programme en cas d'échec.
    function require() {
        fctIn "$*"
        local localName=${1:-''};
        if [[ -z "$localName" ]]; then return $E_ARG_BAD; fi
        if [[ -f "$localName" ]]
        then
            if [[ ! -x "$localName" ]];then evalCmd "chmod +x $localName;"; fi
            #echo "nano $localName # Pour modifier le fichier"
            evalCmd ". $localName";
            local -i lastErrNu=$?
            if [[ $lastErrNu -eq 0 ]]
            then
                notifsAddEcho "Le fichier requis '$localName' est chargé";
                fctOut "$FUNCNAME(0)";return 0;
            fi
            fctOut "$FUNCNAME"; return $lastErrNu;
        fi
        erreursAdd "Le fichier requis '$localName' est introuvable";
        endStandard;
        fctOut "$FUNCNAME($E_REQUIRE)"; exit $E_REQUIRE;
    }
#


# ######## #
# SHOWVARS #
# ######## #
    function showVars() {
        fctIn;
        if [[ $isShowVars -ne 0 ]]
        then
            titre1 "$FUNCNAME();"
            #titre2 'Les Variables'
            #displayVar " argNb=$argNb"
            displayVar 'VERSION' "$VERSION" 'legite-libs' "$LEGITELIB_VERSION"
            displayVar 'LEGITELIB_VERSION' "$LEGITELIB_VERSION"
            displayVar 'SCRIPT_PATH' "$SCRIPT_PATH"
            displayVar 'SCRIPT_REP ' "$SCRIPT_REP"
            displayVar  'SCRIPT_FILENAME' "$SCRIPT_FILENAME" 'SCRIPT_NAME' "$SCRIPT_NAME"  'SCRIPT_EXT' "$SCRIPT_EXT"
            #displayVar 'IFS_DEFAULT' "$IFS_DEFAULT" 'IFS' "$IFS"
            displayVar 'IS_ROOT  ' "$IS_ROOT"  'isUpdate' "$isUpdate"
            displayVar 'verbosity' "$verbosity" "isDebug" "$isDebug" "isShowVars" "$isShowVars" 'isShowDuree' "$isShowDuree"
            displayVar 'LOG_ROOT' "$LOG_ROOT"
            displayVar 'LOG_PATH' "$LOG_PATH"
            displayVar 'isSimulation'  "$isSimulation"
            displayVar 'isShowCollections' "$isShowCollections" "isListeCollections" "$isListeCollections"

            displayVar "isShowLibs" "$isShowLibs" 'isShowCollections' "$isShowCollections" "isListeCollections" "$isListeCollections"

            displayVar 'isLibExecAuto' "$isLibExecAuto";

            displayVar 'tabMotifFct' "$tabMotifFct" 'tabNbFct' "$tabNbFct"
            displayVar 'UPDATE_HTTP_ROOT' "$UPDATE_HTTP_ROOT";
            displayVar 'UPDATE_HTTP     ' "$UPDATE_HTTP";
            displayVar 'CONF_REP        ' "$CONF_REP";
            displayVar 'COLLECTIONS_REP ' "$COLLECTIONS_REP"
            displayVar 'CONF_SRC_PATH   ' "$CONF_SRC_PATH";
            displayVar 'CONF_ORI_PATH   ' "$CONF_ORI_PATH";
            #displayVar 'CONF_FILENAME   ' "$CONF_FILENAME";
            displayVar 'CONF_USR_PATH   ' "$CONF_USR_PATH";
            displayVar 'CONF_PSP_PATH   ' "$CONF_PSP_PATH";

            displayVar 'isCollectionFail' "$isCollectionFail";
            displayVar 'isCollectionExecAuto'    "$isCollectionExecAuto";
            displayVar 'librairiesTbl[index*]'  "${!librairiesTbl[*]}";
            displayVar 'librairiesCall[*]'      "${librairiesCall[*]}";

            showPSPCall
            #titre2 '  libParams:';            displayLibParams
            showLibParams;
            showVarsLocal;
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }

    function showVarsPost() {
        fctIn;        
        if [[ $isShowVars -eq 1 ]]
        then
            titre1 "$FUNCNAME()";
            displayVar 'isCollectionExecAuto' "$isCollectionExecAuto"  'isLibExecAuto' "$isLibExecAuto";
            displayVar 'isCollectionFail' "$isCollectionFail";

            titre2 'showVarsPostLocal()';
            showVarsPostLocal;
        fi
        showDuree;
        fctOut "$FUNCNAME(0)";return 0;
    }


    # a surcharger dans le projet
    function showVarsLocal() {
        fctIn;
        fctOut "$FUNCNAME(0)";return 0;
    }
#


# #################### #
# FONCTION TABULATIONS #
# #################### #
    declare tabMotifFct='-';            # motif de decalage pour chaque sous rep (modifié aussi par setTabulation)
    declare tabulationsFct='';          # cumul du motif 
    declare -i tabNbFct=-1;



    # Construit la chaine des separateur en fonction du nombre dasn la pile des fonctions appellées
    function calcTabulationsFct(){
        if [ $# -ne 1 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi
        local -i _fctNb=$1;
        tabulationsFct='';
        local _i=3;                          # on en enleve 3
        while [[ $_i -lt $_fctNb  ]];do tabulationsFct+="$tabMotifFct";((_i++));  done
        return 0;
    }

    function fctIn() {
        #((tabNbFct++))        setTabulationFct
        #incTabulationFct;
        local -i _verbosity=${2:-0};
        if [[ $isDebug -ge 1 || ($verbosity -ge $_verbosity && $_verbosity -ne 0 ) ]]
        then
            local _fctNb=${#FUNCNAME[*]}
            calcTabulationsFct $_fctNb;     # recalcul de fctTabulations

            local _fctParams="${1:-''}"     # renvoie le nom de la function et non un parametre
            local _fct="${FUNCNAME[1]}";
            local _fctNoms='';
            if [[ $_fctNb -ge 2 ]]
            then
                for (( i=2; i<$_fctNb; i++ )); # on eneleve fctIn et la fonction actuelle
                do
                    _fctNoms+="${FUNCNAME[$i]} ";
                done
            fi
            #if [[ ! -z "$_fctNoms" ]]; then _fctNoms=" [ $_fctNoms]";fi
            echo -e $FUNCTION_COLOR"$tabulationsFct$_fct($_fctParams)[ $_fctNoms]"$NORMAL;
        fi
        return 0;
    }

    function fctOut() {
        local -i _verbosity=${2:-0};
        if [[ $isDebug -ge 1 || ($verbosity -ge $_verbosity && $_verbosity -ne 0 ) ]]
        then
            local _fonctionNameCall="${1:-""}"
            echo "${FUNCTION_COLOR}$tabulationsFct$_fonctionNameCall:END"$NORMAL;
        fi
        #decTabulationFct;
        return 0;
    }
#


#############
# LOAD CONF #
#############
    function loadConfs() {
        fctIn;
        displayDebug " === CHARGEMENT DES FICHIERS DE CONFIGURATION ==="
        execFile "$CONF_SRC_PATH";   # rep script du fichier ori (pour)
        execFile "$CONF_ORI_PATH";
        execFile "$CONF_USR_PATH";
        execFile "$CONF_PSP_PATH";
        loadConfsLocal;
        fctOut "$FUNCNAME(0)";return 0;
    }

    # a surcharger par le projet
    function loadConfsLocal() {
        fctIn;
        #execFile "$SCRIPT_REP/lib.cfg.sh"
        #execFile "$CONF_REP/lib.cfg.sh"
        fctOut "$FUNCNAME(0)";return 0;
    }
#


##################################
# GESTION DES APPELS DES PSPCall #
##################################
    function showPSPCall() {
        fctIn "$*";
        local out='';
        for value in "${PSPCall[@]}";
        do
            out+=" '$value'";
        done;

        local _index=0;
        local _PSPNom='';
        local _out='';
        while :
        do
            if [[ $isTrapCAsk -eq 1 ]];then break;fi
            ((_index++))
            if [ $_index -gt $PSPCallIndex ];then break;fi
            _PSPNom=${PSPCall[$_index]:-""}
            _out="$_out $_PSPNom";
        done

        displayVar 'PSPCall in order' "$_out";
        fctOut "$FUNCNAME(0)";return 0;
    }

    function addPSPCall() {
        fctIn "$*";
        if [ $# -eq 0 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi
        ((PSPCallIndex++))
        PSPCall[$PSPCallIndex]="$1"
        PSPCallNb=$PSPCallIndex;
        fctOut "$FUNCNAME(0)";return 0;
    }


    # execPSPCall
    # interprete les valeurs de PSPCall et charge la collection ou execute la librairie
    function execPSPCall() {
        local -i _fctVerbosity=1;
        fctIn "$*" $_fctVerbosity;

        #titre1 "Chargement des collections et des librairies($FUNCNAME)";
        displayDebug "=== $FUNCNAME ==="
        #displayVar 'PSPCallIndex' "$PSPCallIndex" 'PSPCallNb' "$PSPCallNb"

        local _index=0;
        local _PSPNom='';
        while :
        do
            if [[ $isTrapCAsk -eq 1 ]];then break;fi
            ((_index++))
            if [ $_index -gt $PSPCallIndex ];  then break; fi

            # Le PSP peut etre une (rep/)collection, une lib ou autre chose
            _PSPNom=${PSPCall[$_index]:-""};
            if [[ "$_PSPNom" = '' ]]; then break; fi


            # - Festion du mode automatique - #
            local _auto="${_PSPNom: -5}";                    # laisser 'espace entre '''': et '-4'
            #displayVar '_auto' "$_auto"
            if [[ ${_auto} == '-Auto' ]]                      # executer si ne finit pas par '-Auto'
            then 
                echoV 1 "Mode auto:On";

                setCollectionIntegrale 1;                   # execution complete de la collection (activé via le PSP)
                #addLibrairie "${collectionNom}-Auto";
                _PSPNom=${_PSPNom%-*};                      # suppression du suffixe '-Auto'
                displayVarDebug "$FUNCNAME:$LINENO:_PSPNom" "$_PSPNom";
            else
                echoV 1 "Mode auto:Off";
                # Le PSP peut etre une collection, une lib ou autre chose
                # Afficher la commande uniquement c'est une collection et PAS une lib
                if isCollectionOnly "${_PSPNom}"
                then
                    titreInfo "Lancer '$SCRIPT_FILENAME $_PSPNom-Auto' pour executer les librairies lancées avec libExecAuto";
                fi
            fi

            loadCollection "$_PSPNom";                      # charge la collection (et la lib homonyme)
            if [ $? -ne 0 ]; then execLib "$_PSPNom";fi     # Collection NON chargée (donc lib homonyme non chargé)-> on vérifie si c'est une lib
        done
        fctOut "$FUNCNAME(0) $_fctVerbosity";return 0;
    }

    # renvoie:
    # E_ARG_REQUIRE: si pas d'argument
    # $E_TRUE  si la librairie est appellé via PSPCall (et existe)
    # $E_FALSE si lib non appellé
    function isLibCall(){
        fctIn "$*";
        if [ $# -ne 1 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi
        local _lib="$1";
        for _value in "${PSPCall[@]}";
        do
            if [[ "$_value" == "$_lib" ]]; then fctOut "$FUNCNAME($E_TRUE)"; return $E_TRUE; fi
        done;
        fctOut "$FUNCNAME($E_FALSE)"; return $E_FALSE;
    }

    # execute la librairie $1 si elle est appellé via PSPCALL
    function libExecAuto(){
        local -i _fctVerbosity=1;
        fctIn "$*";
        if [ $# -ne 1 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctVerbosity;return $E_ARG_REQUIRE;fi

        local _libNom="$1";

        isLibCall "$_libNom";
        local _isLibCall=$?;
        displayVar "$LINENO:_isLibCall" "$_isLibCall" 'isCollectionExecAuto' "$isCollectionExecAuto";

        if [[ $isCollectionExecAuto -eq 1 || $_isLibCall -eq $E_TRUE ]] # si la collection est en mode -auto ou que la librairie a été appéllée
        then
            execLib "$_libNom";
        fi
        fctOut "$FUNCNAME(0)" $_fctVerbosity;return 0;
    }
#


###########################
# GESTION DES COLLECTIONS #
###########################
    # - Affiche les collections du repertoire 'collections' - #
    function showCollections() {
        fctIn "isShowCollections=$isShowCollections";
        if [[ $isShowCollections -ne 0 ]]
        then
            titre1 "Les collections ($COLLECTIONS_REP)";
            if [ ! -d "$COLLECTIONS_REP" ]
            then
                erreursAddEcho "$FUNCNAME:Le repertoire '$COLLECTIONS_REP' existe pas.";
                fctOut "$FUNCNAME($E_INODE_NOT_EXIST)";return $E_INODE_NOT_EXIST;
            fi

            function showCollectionsTriees(){

                local _rep="${1%?}";
                echo -n "$INFO${_rep##*/}:$NORMAL";
            
                #local _liste=$(ls --almost-all --ignore-backups -1 ${_rep}/*.sh 2>/dev/null );
                #local _liste=$(ls --almost-all --ignore-backups -1 ${_rep}/*.sh  );
                local _liste=$(ls -1 --ignore-backups  $_rep/*.sh  2>/dev/null );

                local _collection='';
                local _collections=''; #(string) listes de collections dans le repertoire de collection
                local _fileName='';
                local _sorted='';

                for _collection in $_liste
                do
                    #echo $collecion
                    _fileName=${_collection##*/};         # nom.ext
                    _collections="$_collections ${_fileName%.sh}";
                done

                IFS=$'\n';
                _sorted="$(sort <<< "${_collections[*]}")";
                IFS="$IFS_DEFAULT";
                
                echo "$_sorted";
                return 0;
            }
            #displayVar '$COLLECTIONS_REP' "$COLLECTIONS_REP";
            showCollectionsTriees "$COLLECTIONS_REP/";

            # -Affiche par sous repertoire - #
            #On recupere la liste des repertoires
            local -a _repBrutTbl=();
            local -i _i=0;
            for d in $COLLECTIONS_REP/*/
            do
                if [ -d "$d" ]
                then
                    showCollectionsTriees "$d";
                fi
            done



        fi
        fctOut "$FUNCNAME(0)";return 0;
    }


    # Charge une collection si le fichier homonyme.sh existe dans $COLLECTIONS_REP
    # charge autommatiquement la librairie homonyme si elle est défini par addLibrairie
    # sauf si --no-auto-lib=1 est défini
    # code retour:
    # 0: La collection est chargé
    # $E_FALSE: erreur
    function loadCollection() {
        fctIn "$*" 1;
        if [[ $# -ne 1 ]];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi
        echoV 1 "$FUNCNAME($*)";

        if [[ $isTrapCAsk -eq 1 ]];then break;fi

        local _retour=$E_FALSE;
        collectionNom="$1";     # global
        #displayVar 'collectionNom' "$collectionNom";

        #isCollectionExist "_PSPNom";    local _isCollectionExist=$?;
        #isCollectionOnly "_PSPNom";     local _isCollectionOnly=$?;
        #isLibExist "_PSPNom";           local _isLibExist=$?;
        #isLibOnly "_PSPNom";            local _isLibOnly=$?;

        # Est-ce une librairie SEULE SANS collection?
        if isLibOnly "$collectionNom"
        then
            #echoV 2 "C'est une librairie seule (c'est pas une collection): on ne charge pas!";
            fctOut "$FUNCNAME($E_FALSE)"; return $E_FALSE;
        fi


        #local collectionPath="$COLLECTIONS_REP/$collectionNom.sh";

        if isCollectionExec "$collectionNom"
        then
            #echoV 2 "La collection '$collectionNom' a déjà été chargée.";
            fctOut "$FUNCNAME(0)";return 0; 
        fi


        isCollectionExist "$collectionNom";
        if [ $? -eq $E_TRUE ]
        then # la collection existe


            # Chargement de la configuration du repertoire
            local _repNom="${collectionNom%/*}";
            #displayVar '_repNom' "$_repNom";
            execFile "$_repNom/_conf.cfg.sh";


            # Chargement du fichier de configuration de la collection
            loadCollectionCfg "$collectionNom";

            
            # Chargement de la collection
            local collectionPath="$COLLECTIONS_REP/$collectionNom.sh";
            execFile "$collectionPath";
            _retour=$?;
            setCollectionExec "$collectionNom" 1;       # La collection est chargée..
            if [ $_retour -eq 0 ]
            then # la collection est chargée

                # Execution de la librairie homonyme (autoexecution)
                echoV 1 "Execution de la librairie homonyme (autoexecution)";

                #displayVar "$LINENO:isCollectionExecAuto" "$isCollectionExecAuto";

                #displayVar 'isCollectionExecAuto' "$isCollectionExecAuto";
                #displayVar 'isLibExecAuto' "$isLibExecAuto";
                if [ $isLibExecAuto -eq 1 ]
                then
                    isLibExist "$collectionNom";
                    if [ $? -eq $E_TRUE ]
                    then
                        _lib="${collectionNom##*/}";
                        #displayVar "Execution automatique de la librairie homonyme ($_lib)";
                        execLib "$_lib";
                        _retour=$?; # pas d'erreur: la lib est executé (ou non)
                    fi
                fi
            else
                erreursAddEcho "Erreur lors du chargement de la collection existante '$collectionPath'"
            fi
        fi
        fctOut "$FUNCNAME($_retour)" 1; return $_retour;
    }


    #isCollectionOnly ( 'nom' )    #collection ou lib
    # renvoie $E_TRUE si l'argement est une collection MAIS PAS une collection
    function isCollectionOnly(){
        if [[ $# -ne 1 ]];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi
        local _collectionNom="$1";
        isCollectionExist "$_collectionNom";    local -i _isCollectionExist=$?;
        isLibExist "$_collectionNom";           local -i _isLibExist=$?;
    
        #displayVar '_collectionNom' "$_collectionNom" '_isCollectionExist' "$_isCollectionExist" '_isLibExist' "$_isLibExist";

        if [[ _isCollectionExist -eq $E_TRUE && _isLibExist -ne $E_TRUE ]]
        then
            return $E_TRUE;
        fi
        return $E_FALSE;
    }

    #isLibOnly ( 'nom' )    #collection ou lib
    # renvoie $E_TRUE si l'argement est une librairie MAIS PAS une collection
    function isLibOnly(){
        if [[ $# -ne 1 ]];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi
        local _collectionNom="$1";
        isCollectionExist "$_collectionNom";    local -i _isCollectionExist=$?;
        isLibExist "$_collectionNom";           local -i _isLibExist=$?;
    
        #displayVar '_collectionNom' "$_collectionNom" '_isCollectionExist' "$_isCollectionExist" '_isLibExist' "$_isLibExist";

        if [[ _isCollectionExist -ne $E_TRUE && _isLibExist -eq $E_TRUE ]]
        then
            return $E_TRUE;
        fi
        return $E_FALSE;
    }

    # loadCollectionCfg ( 'collectionNom' )
    function loadCollectionCfg(){
        fctIn "$*" 1;

        if [[ $# -ne 1 ]];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi

        if [[ $isTrapCAsk -eq 1 ]];then break;fi


        local _retour=$E_FALSE;

        ################################################
        # verifié que l'appeller n'est pas une lib seul #
        ################################################
        if isLibOnly
        then
            echoV 2 "'$_collectionNom' est seulement une lib (mais pas une collection).";
            fctOut "$FUNCNAME(0)";return 0; 
        fi

        #######################################
        # appelle du fichier de configuration #
        #######################################
        local _collectionNom="$1.cfg";  # AJOUTE L'EXTENTION .cfg
        local collectionPath="$COLLECTIONS_REP/$_collectionNom.sh";


        if isCollectionExec "$_collectionNom"
        then
            echoV 1 "La configuration de la collection '$_collectionNom' a déjà été chargée.";
            fctOut "$FUNCNAME(0)";return 0; 
        fi


        if isCollectionExist
        then # la collection existe
            execFile "$collectionPath";     # Execution de la collection
            _retour=$?;
            if [ $_retour -eq 0 ]
            then # la collection est chargée
                echoV 1 "La configuration de la collection '$_collectionNom' est chargée.";
            else
                erreursAddEcho "Erreur lors du chargement de la collection existante '$collectionPath'"
            fi
        fi

        fctOut "$FUNCNAME(0)" 1;return 0;
    }


    # Verifie l'existance du fichier collection donné en param et le rend lisible et executable si neccessaire
    # renvoie E_TRUE si le parametre est un fichier de collection lisible et executable
    # sinon $E_INODE_NOT_EXIST, $E_INODE_NOT_R, E_INODE_NOT_X
    function isCollectionExist(){
        fctIn "$*";
        if [[ $# -eq 0 ]]; then fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fi
        local _collection="$1"
        local _collectionPath="$COLLECTIONS_REP/$_collection.sh"
        if [[ ! -f "$_collectionPath"  ]]
        then
            #echoV 2 "La collection '$_collectionPath' existe pas"  # PAS D'echo c'est au programme appelant a gérer l'erreur
            fctOut "$FUNCNAME($E_INODE_NOT_EXIST)"; return $E_INODE_NOT_EXIST;
            fi
        if [[ ! -r "$_collectionPath"  ]]
        then
            chmod +r "$_collectionPath";
            if [ $? -ne 0 ]
            then
                erreursAddEcho "La collection '$_collection' ne peut être lu (+r)"
                fctOut "$FUNCNAME($E_INODE_NOT_R)"; return $E_INODE_NOT_R;
            fi
        fi
        if [[ ! -x "$_collectionPath"  ]]
        then
            chmod +x "$_collectionPath";
            if [ $? -ne 0 ]
            then
                erreursAddEcho "La collection '$_collection' ne peut être executé (+x)"
                fctOut "$FUNCNAME($E_INODE_NOT_X)"; return $E_INODE_NOT_X;
                fi
        fi

        fctOut "$FUNCNAME($E_TRUE)"; return $E_TRUE; #la collection existe et est exxecutable
    }


    function showCollectionFail(){
        fctIn "$*";
        if [[ isCollectionFail -eq 1 ]]
        then
            titreWarn "La collection '$collectionNom' a eu une erreur critique). Les librairies de cette collection son désactivées.";
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }

    function setCollectionFail(){
        fctIn "$*";
        local -i _is=${1:-1};
        isCollectionFail=$_is;
        displayVarDebug 'isCollectionFail' "$isCollectionFail";
        fctOut "$FUNCNAME(0)";return 0;
    }

    # --- #
    function showModeCollectionIntegrale(){
        fctIn "$*";
        if [[ isCollectionExecAuto -eq 0 ]]
        then
            titreInfo "Le mode collection intégrale est désactivé (Lancer $SCRIPT_FILENAME ${collectionNom}-Auto pour l'activer).";
        else
            titreInfo 'Le mode collection intégrale est activé.';
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }


    function setCollectionIntegrale(){
        fctIn "$*";
        local -i _is=${1:-1};
        isCollectionExecAuto=$_is;
        displayVar 'isCollectionExecAuto' "$isCollectionExecAuto";
        fctOut "$FUNCNAME(0)";return 0;
    }
#

##########################################
# TABLEAU DES COLLECTIONS DEJA EXECUTÉES # 
##########################################

    # Indique que la collection a déjà été executée (ou non).
    # $1: nom de la lib
    # $2(option): valeur 0|1
    function setCollectionExec(){
        fctIn "$*";
        if [ $# -eq 0 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi

        local   _collectionNom="$1";    # requis
        local -i _value="${2:-1}";      # setter a 1 si non précisé
        collectionExecTbl["$_collectionNom"]=$_value;

        fctOut "$FUNCNAME(0)";return 0;
    }

    # renvoie $E_TRUE (0) si la collection a deja été executé
    # $1: nom de la collection
    function isCollectionExec(){
        fctIn "$*";
        if [ $# -ne 1 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi

        local   _collectionNom="$1";         # requis
        # Si la lib n'a pas deja été exécutée alors renvoie faux
    
        if [[ ${libExecTbl["$_collectionNom"]:-0} -eq 0 ]];then fctOut "$FUNCNAME($E_FALSE)"; return $E_FALSE; fi
        fctOut "$FUNCNAME($E_TRUE)"; return $E_TRUE;
    }
#


##########################
# GESTION DES LIBRAIRIES #
##########################
    # - Affiche les noms des librairies - #
    function showLibs() {
        fctIn "isShowLibs=$isShowLibs";

        if [[ $isShowLibs -eq 0 ]];then fctOut "$FUNCNAME(0)";return 0;fi

        local librairiesTblIndex=''; # liste des index du tableau librairiesTbl (trié)
        IFS=$'\n';
        librairiesTblIndex=($(sort <<< "${!librairiesTbl[*]}"));
        IFS="$IFS_DEFAULT";

        if [[ $isShowLibs -eq 1 ]]
        then
            titre1 'Librairies possibles'
            local out='';
            for value in "${librairiesTblIndex[@]}";
            do
                out+=" $value"
            done;
            echo "$out";
        elif [[ $isShowLibs -ge 2 ]]
        then
            titre3 'Librairies possibles'
            for value in "${librairiesTblIndex[@]}";# la valeur est la clef du tableau librairiesTbl
            do
                local description=${librairiesDescr["$value"]:-''}
                displayVar "$value" "$description"
            done;
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }


    # renvoie:
    # E_ARG_REQUIRE: si pas d'argument
    # $E_FALSE si lib == ""
    # $E_TRUE  si lib existe
    # $E_FALSE si lib non existe
    function isLibExist() {
        fctIn "$*";
        if [[ $# -eq 0 ]]; then fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fi
        #local _lib="$1";
        #displayVar '1' "$1";
        local _lib="${1##*/}";        # supprimmer les sous rep (domaine)
        #displayVar 'Est ce que la lib existe?' "$_lib";
        if [[ "$_lib" == '' ]];then fctOut "$FUNCNAME($E_FALSE)"; return $E_FALSE;fi
        for key in "${!librairiesTbl[@]}";
        do
            if [[ "$key" == "$_lib" ]]; then fctOut "$FUNCNAME($E_TRUE)"; return $E_TRUE; fi
        done;
        #echo 'Non la lib existes existe pas';
        fctOut "$FUNCNAME($E_FALSE)"; return $E_FALSE;
    }


    # addLibrairie( 'libNom' ['description'] ['libCmd'=libNom])
    function addLibrairie() {
        local -i _fctVerbosity=1;
        fctIn "$*" $_fctVerbosity;
        if [[ $# -eq 0 ]];then erreursAddEcho "${BASH_SOURCE}:$LINENO:$FUNCNAME('libNom' ['description'] ['libCmd'=libNom]) ($*). Appellé par ${FUNCNAME[1]}()";       fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctVerbosity; return $E_ARG_REQUIRE;fi

        local _libNom="$1";
        if [[ "$_libNom" == '' ]];then return $E_ARG_BAD;fi
        local _libDescr=${2:-''};
        local _libCmd="${3:-"$_libNom"}";
        librairiesTbl["$_libNom"]="$_libCmd";
        librairiesDescr["$_libNom"]="$_libDescr";
        setLibExec "$_libNom" 0;           # La lib n'a pas encore été appelllée.
        #echoV 2 "Ajout de la librairie '$_libNom'.";
        fctOut "$FUNCNAME(0)" $_fctVerbosity;return 0;
    }


    #exeLib ( 'libNom' )
    # execute la librairie si existante (et si pas deja executé)
    # retour:
    # 0: la librairie existe et a été executée
    # $E_FALSE: ereur
    function execLib() {
        local -i _fctVerbosity=1;
        if [[ $isTrapCAsk -eq 1 ]];then return $E_CTRL_C;fi
        fctIn "$*" $_fctVerbosity;
        if [[ $# -eq 0 ]]; then erreursAddEcho "${BASH_SOURCE}:$LINENO:$FUNCNAME( 'libNom' ) ($*). Appellé par ${FUNCNAME[1]}()"; fctOut "$FUNCNAME($E_ARG_REQUIRE)" $_fctVerbosity; return $E_ARG_REQUIRE;fi

        local _libNom="$1";

        if [[ $isCollectionFail -eq 1 ]]
        then
            erreursAddEchoV  "La librairie  '$_libNom' est désactiviée du à une erreur de la collection";
            fctOut "$FUNCNAME($E_FALSE)" $_fctVerbosity; return $E_FALSE;
        fi

        if isLibExec "$_libNom"
        then
            echoV 2 "La librairie  '$_libNom' a déjà été executée";
            fctOut "$FUNCNAME($E_FALSE)" $_fctVerbosity; return $E_FALSE;
        else
            echoV 2 "La librairie '$_libNom' n'a jamais été exécutée."
        fi

        isLibExist "$_libNom";
        if [[ $? -eq $E_TRUE ]]
        then
            titreLib "$_libNom";
            eval "${librairiesTbl["$_libNom"]}"; #eval simple et non evalCmd
            setLibExec "$_libNom" 1;
            fctOut "$FUNCNAME(0)";return 0;
        fi
        fctOut "$FUNCNAME($E_FALSE)" $_fctVerbosity; return $E_FALSE;
    }
#


#########################################
# TABLEAU DES LIBRAIRIES DEJA EXECUTÉES # 
#########################################

    # Indique que la lib a déjà été executée (ou non).
    # $1: nom de la lib
    # $2(option): valeur 0|1
    function setLibExec(){
        fctIn "$*";
        if [ $# -eq 0 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi

        local   _libNom="$1";         # requis
        local -i _value="${2:-1}";     # setter a 1 si non précisé
        libExecTbl["$_libNom"]=$_value;

        fctOut "$FUNCNAME(0)";return 0;
    }

    # renvoie $E_TRUE (0) si la librairie a deja été executé
    # $1: nom de la lib
    function isLibExec(){
        fctIn "$*";
        if [ $# -ne 1 ];then fctOut "$FUNCNAME($E_ARG_REQUIRE)";return $E_ARG_REQUIRE;fi

        local   _libNom="$1";         # requis
        # Si la lib n'a pas deja été exécutée alors renvoie faux
        #displayVar '${libExecTbl["$_libNom"]' "${libExecTbl["$_libNom"]}";
        if [[ ${libExecTbl["$_libNom"]:-0} -eq 0 ]];then fctOut "$FUNCNAME($E_FALSE)"; return $E_FALSE; fi
        fctOut "$FUNCNAME($E_TRUE)"; return $E_TRUE;
    }
#


#############
# PSPParams #
#############
    # Ajoute le parametre (et une seul) dans libParams[] (qui sont apres '--')
    function addLibParams(){
        fctIn "$*";
        local param="$1";
    
        if [[ "$param" == *'='* ]]  # y a til un "=" -> key=valeur?(substr)
        then
            displayDebug "La parametre $param contient '=' -> on split"
            IFS='=';read -ra paramDatas <<< "$param";IFS="$IFS_DEFAULT";
            libParams["${paramDatas[0]}"]="${paramDatas[1]}";
            unset paramDatas;
        else
            libParams["$1"]=1;
        fi
        #displayLibParams
        fctOut "$FUNCNAME(0)";return 0;
    }

    function displayLibParams(){
        fctIn;
        for key in "${!libParams[@]}"; do
            displayVar "$key" "${libParams[$key]}"
        done
        fctOut "$FUNCNAME(0)";return 0;
    }


    # Affiche sur une ligne les parametre de lib
    function showLibParams(){
        fctIn;
        local _datas='';
        for key in "${!libParams[@]}"; do
            _datas="$_datas,'$key':'${libParams[$key]}'"
        done
        echo "libParams[@]='${_datas#?}'"; # supppresion du 1er car(',')
        fctOut "$FUNCNAME(0)";return 0;
    }
#


##################
# ECHO VERBOSITY #
##################
    #echoV verbositéDuText 'texte a afficher'
    function echoV(){
        if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE; fi
        #if [[ $verbosity -eq 0 || $vic -eq 0 ]];then return 0;fi
        if [[ $verbosity -eq 0  ]];then return 0;fi

        local -i _v="${1:-$vic}";
        #displayVar '_v' "$_v";

        if [[ $_v -le $verbosity ]]
        then
            echo "$2";
        fi
        return 0;
    }

#


##################
# NORMALIZE TEXT #
##################
    declare normalizeText='';               # variable de resultat de la fonction normalizeTexte()
    # prend une chaine et renvoie une chaine en normalisant les caracteres
    function getNormalizeText() {
        #fctIn "$*";
        if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE; fi

        # syntax de substitution POSIX
        # variable=${variable/source_qui_sera_remplacé/nouveau_qui remplacera}
        normalizeText="$1"; # global

        normalizeText=$(echo $normalizeText | tr -d '?');
        normalizeText=$(echo $normalizeText | tr -d '!');
        normalizeText=$(echo $normalizeText | tr -d '$');
        normalizeText=$(echo $normalizeText | tr -d '@');
        #normalizeText=$(echo $normalizeText | tr -d '#');
        
        normalizeText=$(echo $normalizeText | sed 's/[âàä]/a/g');
        normalizeText=$(echo $normalizeText | sed 's/[ÂÀÂ]/a/g');
        normalizeText=$(echo $normalizeText | sed 's/[éèêë]/e/g');
        normalizeText=$(echo $normalizeText | sed 's/[ÉÈÊË]/E/g');
        normalizeText=$(echo $normalizeText | sed 's/[îï]/i/g');
        normalizeText=$(echo $normalizeText | sed 's/[ÎÏ]/I/g');
        normalizeText=$(echo $normalizeText | sed 's/[ôö]/o/g');
        normalizeText=$(echo $normalizeText | sed 's/[ÔÖ]/O/g');
        normalizeText=$(echo $normalizeText | sed 's/[ûüù]/u/g');
        normalizeText=$(echo $normalizeText | sed 's/[ÛÜÙ]/U/g');
        normalizeText=$(echo $normalizeText | sed 's/[çÇ]/c/g');

        normalizeText=$(echo $normalizeText | tr "œ" 'oe');
        normalizeText=$(echo $normalizeText | tr "Œ" 'OE');

        # avant '-'
        normalizeText=$(echo $normalizeText | tr '’' '_');
        normalizeText=$(echo $normalizeText | tr '′' '_');
        normalizeText=$(echo $normalizeText | tr "'" '_');
        normalizeText=$(echo $normalizeText | sed 's/  */_/g');
        normalizeText=$(echo $normalizeText | sed -e "s/[ °]/_/g");

        # '-'
        normalizeText=$(echo $normalizeText | tr '–' '-');
        normalizeText=$(echo $normalizeText | tr '®' '-'); #(R)
        normalizeText=$(echo $normalizeText | tr ":" '-');
        normalizeText=$(echo $normalizeText | tr '"' '-');
        normalizeText=$(echo $normalizeText | tr '[' '-');
        normalizeText=$(echo $normalizeText | tr ']' '-');
        normalizeText=$(echo $normalizeText | tr '«' '-');
        normalizeText=$(echo $normalizeText | tr '»' '-');
        normalizeText=$(echo $normalizeText | tr '–' '-');
        normalizeText=$(echo $normalizeText | sed -e "s/[\@\~#\^\:\`\{\}\(\)\|\,\=]/-/g");
        #normalizeText=$(echo $normalizeText | sed -e "s/@=]/-/g");
        
        normalizeText=$(echo $normalizeText | tr '&' '_et_');

        normalizeText=$(echo $normalizeText | sed -e 's/--*\././g');
        normalizeText=$(echo $normalizeText | sed -e 's/__*\././g');

        normalizeText=$(echo $normalizeText | sed 's/__*-__*/-/g');
        normalizeText=$(echo $normalizeText | sed 's/_-/-/g');
        normalizeText=$(echo $normalizeText | sed 's/-_/-/g');

        normalizeText=$(echo $normalizeText | sed 's/\._*/./g');

        normalizeText=$(echo $normalizeText | sed 's/__*/_/g');
        normalizeText=$(echo $normalizeText | sed 's/--*/-/g');
        
        normalizeText=${normalizeText%-}    # suppression du caractere de fin: '-'
        normalizeText=${normalizeText%_}    # suppression du caractere de fin: '_'

        # ce caractere est crée par 
        #normalizeText=$(echo $normalizeText | tr -d '�');
        #fctOut "$FUNCNAME(0)";return 0;
    }


    # Utilisation: texte_normaliser=$(echoNormalizeText "texte a normaliser")
    function echoNormalizeText(){
        #fctIn "$*";
        #if [[ $# -eq 0 ]];then return $E_ARG_REQUIRE; fi
        getNormalizeText "$1"
        echo "$normalizeText" # Attention sou la forme 
        #displayVar 'normalizeText in echoNormalizeText()' "$normalizeText";
        #fctOut "$FUNCNAME";
        return 0;
    }
#


#################
# FILE HORODATE #
#################
    declare fileHorodated='';
    # fileHorodate 'txt'
    fileHorodate() {
        fctIn "$*";

        #local d=$(date +"%Y-%m-%d-%H-%M")
        fileHorodated="$1-"$(date +"%Y-%m-%d-%H-%M")
        fctOut "$FUNCNAME(0)";return 0;
    }

    #splitHeure ([hh:][mm:]ss)
    # split le parametre et set splitHHMMSS_hh,splitHHMMSS_mm,splitHHMMSS_ss et splitHHMMSS_str
    declare -i splitHHMMSS_hh=0;
    declare -i splitHHMMSS_mm=0;
    declare -i splitHHMMSS_ss=0;
    declare -i splitHHMMSS_secondes=0
    declare    splitHHMMSS_str='';


    function splitHHMMSS() {
        fctIn "$*";
        #if [ $isTrapCAsk -eq 1 ]; then return $E_CTRL_C ; fi
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${BASH_SOURCE}:$LINENO:$FUNCNAME('[hh:][mm:]ss')($*). Appellé par ${FUNCNAME[1]}()"
            fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        displayVarDebug 'arg' "$#"

        local _HHMMSS="$1"
        local _HHMMSSTbl=($(echo $_HHMMSS | tr ":" "\n"))
        local _HHMMSSTblNb=${#_HHMMSSTbl[@]}

        # - global - #
        splitHHMMSS_hh='';splitHHMMSS_mm='';splitHHMMSS_ss='';splitHHMMSS_str='';

        # - leadind zero - #
        local _HH='';
        local _MM='';
        local _SS='';

        if [[ $_HHMMSSTblNb -eq 1 ]]
        then
            splitHHMMSS_ss=${_HHMMSSTbl[0]}
            strLz $splitHHMMSS_ss; _SS="$lz"
            splitHHMMSS_str="$_SS"
        elif [ $_HHMMSSTblNb -eq 2 ]
        then
            splitHHMMSS_mm=${_HHMMSSTbl[0]};
            splitHHMMSS_ss=${_HHMMSSTbl[1]}
            strLz $splitHHMMSS_mm; _MM="$lz"
            strLz $splitHHMMSS_ss; _SS="$lz"
            splitHHMMSS_str="$_MM:$_SS"
        elif [ $_HHMMSSTblNb -eq 3 ]
        then
            splitHHMMSS_hh=10#${_HHMMSSTbl[0]};
            splitHHMMSS_mm=10#${_HHMMSSTbl[1]}; # forcer la base 10 sinon pb avec 8x
            splitHHMMSS_ss=10#${_HHMMSSTbl[2]};
            strLz $splitHHMMSS_hh; _HH="$lz"
            strLz $splitHHMMSS_mm; _MM="$lz"
            strLz $splitHHMMSS_ss; _SS="$lz"
            splitHHMMSS_str="$_HH:$_MM:$_SS"
        fi
        splitHHMMSS_secondes=$(( splitHHMMSS_hh *3600 + splitHHMMSS_mm * 60 + splitHHMMSS_ss ))

        displayVar '_HHMMSS ' "$_HHMMSS"
        displayVar '_HHMMSSTbl[@]' "${_HHMMSSTbl[@]}"
        displayVar 'splitHHMMSS_hh' "$splitHHMMSS_hh" '_HH' "$_HH"
        displayVar 'splitHHMMSS_mm' "$splitHHMMSS_mm" '_MM' "$_MM"
        displayVar 'splitHHMMSS_ss' "$splitHHMMSS_ss" '_SS' "$_SS"
        displayVar 'splitHHMMSS_secondes' "$splitHHMMSS_secondes"
        displayVar 'splitHHMMSS_str' "$splitHHMMSS_str"

        fctOut "$FUNCNAME(0)";return 0;
    }
#


################
# LEADING ZERO #
################
    #strLz( string )
    declare lz='';
    function strLz() {
        fctIn "$*";
        if [[ $# -ne 1 ]]
        then
            erreursAddEcho "${BASH_SOURCE}:$LINENO:$FUNCNAME($@)  (\"codeYoutube\"). Appellé par ${FUNCNAME[1]}()"
            fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;
        fi
        lz='';
        local str="$1"
        # https://neologfr.blogspot.com/2007/12/value-too-great-for-base.html
        local -i chiffre=10#$str; #forcer la base 10 # le format de chiffre 0n est codé en octal

        #displayVar 'str' "$str"
        #displayVar 'chiffre' "$chiffre"

        lz="$chiffre";
        if [[ $chiffre -lt 10 ]]
        then
            lz="0$chiffre";
        fi
        #displayVar 'lz' "$lz"
        fctOut "$FUNCNAME(0)";return 0;
    }

    # dupliqueNCar duplique $1 fois le car $2
    function dupliqueNCar(){
        if [[ $# -ne 2 ]];then erreursAddEcho "$FUNCNAME[$?/2](Nbre_de_car 'C') ($*)"  fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fi
        local -i _N=$1;
        local C="$2"
        local _out=''
        for ((i = 0 ; i < $_N ; i++)); do _out="$_out$C";done
        echo "$_out"
        return 0;
    }
#


# ##### #
# USAGE #
# ##### #
    function usage() {
        fctIn;
        if [[ $argNb -eq 0 ]]
        then
            titreUsage "$SCRIPT_FILENAME [-d] [--showVars] [--showLibs] [--update] [--conf=fichier.cfg.sh] collection|lib - paramLib1 paramLib2=valeur";
            echo "--update: update le programme legite-scripts.sh lui meme (via $UPDATE_HTTP)."
            echo '--conf: execute un fichier de configuration si existe. Peut être appellé plusieurs fois pour charger plusieurs fichiers.'
            echo 'Fichier de configurations:      (visible avec --showVars).'
            echo 'collection|lib: execute une collection ou librairie (visible avec --showCollections|--showLibs).'
            isShowCollections=1; showCollections
            isShowLibs=1;        showLibs;
            usageLocal
            exit $E_ARG_NONE;
        fi
        fctOut "$FUNCNAME(0)";return 0;
    }

    # A surcharger par le projet
    function usageLocal() {
        fctIn;

        fctOut "$FUNCNAME(0)";return 0;
    }
#


##########
# DIVERS #
##########
    #https://www.shellscript.sh/tips/echo/

    # Certain systeme n'interprete pas le le parametre -n  de echo mais utilise a la place \c en fin de chaine
    # definit les variables globales n et c selon si le systeme interprete le parametre -n  de echo
    # exemple d'utilisation: echo $n "Enter your name: $c"
    #if [ "$(echo -n)" = '-n' ]
    #then n='';   c="\c";
    #else n="-n"; c=''
    #fi

    # showFreedisk 'rep1 'rep2' ...
    function showFreedisk() {
        fctIn "$*";
        if [[ $# -eq 0 ]];then fctOut "$FUNCNAME($E_ARG_REQUIRE)"; return $E_ARG_REQUIRE;fi

        titre3 'Espace libre restant'

        for rep in "$@"
        do
            if [[ ! -e "$rep" ]];then continue;fi

            #afficher l'espace libre du repertoire donné en parametre
            local _df_result=$(df -h "$rep" | grep /)
            _df_result=$(echo "$_df_result" | sed 's/  */ /g') # Ne laisser qu'un espace entre chaque terme
            #displayVarDebug 'df_result' "$_df_result"

            local _df_support="$(echo "$_df_result"   | cut --delimiter=' ' --fields=1)"
            #displayVarDebug '_df_support' "$_df_support"

            local _df_taille="$(echo "$_df_result"    | cut --delimiter=' ' --fields=2)"
            #displayVarDebug '_df_taille' "$_df_taille"

            #local _df_utiliser=$(echo "$_df_result" | cut --delimiter=" " --fields=3)
            #displayVarDebug '_df_utiliser' "$_df_utiliser"

            local _df_libre="$(echo "$_df_result"     | cut --delimiter=' ' --fields=4)"
            #displayVarDebug '_df_libre' "$_df_libre"

            #_df_utiliserPct="$(echo "$df_result" | cut --delimiter=' '' --fields=5)""
            #displayVarDebug '_df_utiliserPct' "$_df_utiliserPct"

            local _df_monterSur="$(echo "$_df_result" | cut --delimiter=' ' --fields=6)"
            #displayVarDebug '_df_monterSur' "$_df_monterSur"

            #echo  "${INFO}$_df_support${NORMAL}(${INFO}$_df_monterSur${NORMAL}) Libre: $INFO$_df_libre$NORMAL/$INFO$_df_taille$NORMAL"
            displayVar "$_df_monterSur" "$_df_support" 'Libre' "$_df_libre" 'Taille' "$_df_taille"
        done
        fctOut "$FUNCNAME(0)";return 0;
    }

#


########
# MAIN #
########
    function beginStandard() {
        #if [ $isDebug -eq 1 ];then clear;fi
        fctIn;

        echo "${INFO}$SCRIPT_PATH $VERSION$NORMAL";
        trapInit;
        loadConfs; # Avant usage()
        usage;
        selfUpdate;
        showVars;
        showCollections;

        execPSPCall;
            #execCollectionsCall # les collections ne sont pas executées. Ce sont leur lib qui le sont
            #execLibrairiesCall

        showLibs;  #afficher une fois toutes les libs chargées (apres loadConfs)


        fctOut "$FUNCNAME(0)";return 0;
    }

    function endStandard() {
        fctIn;
        #echo "Résumé: "
        notifsShow;
        erreursShow;
        trapErrShow;
        showVarsPost;
        fctOut "$FUNCNAME(0)";return 0;
    }
#
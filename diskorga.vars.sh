#!/bin/bash

if [[ ${IS_LEGITELIB_CALL:-0} -eq 0 ]]
then
    echo "Ce fichier n'est pas destiné à être chargé directement.";
    echo "La librairie legite-lib.sh n'est pas chargé.";
    exit 1;
fi


# ---- gestion des caches ---- #
#repertoire temporaire
if [[ -w "/dev/shm" ]]
then
    declare -r REP_TEMP="/dev/shm";
else
    declare -r REP_TEMP="/tmp";
fi

declare -i actionNb=0;                  # nb d'action demandé
declare cachePrefixe="$REP_TEMP/$SCRIPT_NAME"    # $cachePrefixe-dir.txt $cachePrefixe-file.txt (Attention n'est pas un repertoire)
declare -i isKeepCache=0;               # garder le fichier de cache (ne pas le supprimer)
declare -i isCreateCache=0;             # crée seulement le cache (ne fait aucune autre operation, met isKeepCache a 1)
declare -i isFromCache=0;               # ne (re)crée pas le cache, utilise les caches donnés en prefixe
declare cacheName='';                   # nom du cache actuel (calculer selon dir/file)

declare -i isTest=0;
declare -r testDir="$REP_TEMP/test/$SCRIPT_NAME";

declare -A filtres;                     # tableau contenant les fonctions de filtre
declare filtreNo="";                    # indique le nom du filtre actif
#declare filtresActifs="";              # filtres qui ont été activés 
declare repertoire_source='';           # repertoire INITIAL contenant les fichiers a renommer defini en argument ou a copier
declare repertoire_destination='';      # repertoire ou sera copié le repertoire source. uitilisé uniquement si -c présent
declare tabulationMotif='-';            # motif de decalage pour chaque sous rep (modifié aussi par setTabulationDl)
declare tabulation='';                  # cumul du motif 
declare -i repertoireTotal=0;   # nombre total de repertoire scannés

declare fichierMotif=''        # motif de recherche pour les fichiers

declare -i profondeurNb=0;     # niveau de repertoire (le rep de base est compté)
declare -r PROFONDEUR_MAX=9;   # 0: desactive; 1: que le rep root 2: root+1 rep; etc
                    # ne pas depasser 9 car lit 10 comme 1

declare -i inodeRepTotal=0;declare -i inodeRepLu=0;declare -i inodeRepPct=0;
declare -i inodeFicTotal=0;declare -i inodeFicLu=0;declare -i inodeFicPct=0;
declare -A inodeCompteur; #tableau associatif
declare -i inodeCompteur['repertoires_lus']=0;
declare -i inodeCompteur['repertoires_modifies']=0;
declare -i inodeCompteur['repertoires_modifies_total']=0;
declare -i inodeCompteur['fichiers_lus']=0;
declare -i inodeCompteur['fichiers_modifies']=0;
declare -i inodeCompteur['fichiers_modifies_total']=0;
declare -i inodeCompteur['fichiers_supprimes']=0;
declare -i inodeCompteur['liens']=0;
declare -i inodeCompteur['repertoires_copies']=0;
declare -i inodeCompteur['repertoires_non_copies']=0;
declare -i inodeCompteur['repertoires_copies_existant']=0;
declare -i inodeCompteur['fichiers_copies']=0;
declare -i inodeCompteur['fichiers_non_copies']=0;
declare -i inodeCompteur['fichiers_copies_existant']=0;
declare -i inodeCompteur['convert_exist']=0;
declare -i inodeCompteur['convert_ok']=0;
declare -i inodeCompteur['convert_fail']=0;

# tag interne
declare -i isDeleted=0;      # fichier a supprimé (utiliser par les filtres)

# tag de comportement
declare -i isRecursif=0;
declare -i isDironly=0;      # ne copier que les repertoires
declare -i isFileonly=0;     # ne copier que les fichiers
declare -i isTrapCAsk=0;     # demande d'interuption par control C

# FONCTIONS #
declare -i isCopie=0;        # Le fonctiond de copie doit elle être appelé a la place de la fonction renommer?
declare -i isRename=0;       # desactiver la fonction de renommage
declare -i isCompare=0;      # compare la presence/absence des fichiers/rep entre source et destination
declare -i isCompareStats=0  # comparer les sats de source et destination
declare -i isConvert=0       # convertit le repertoire donnée defuat './'
declare    convertFrom="m4a"
declare    convertTo="ogg"

declare -i isStats=0;        # afficher les stats des repertoires


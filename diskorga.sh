#!/bin/bash
set -u;
#IFS=$'\n\t' # definir l'IFS
# diskorga.sh
# renomme les fichier du repertoire donné en argument
# de facon recursive
# en ne suivant pas les liens symboliques
declare -r SCRIPT_PATH=$(realpath $0);         # chemin  absolu complet du script rep + nom
declare -r SCRIPT_REP=$(dirname $SCRIPT_PATH); # repertoire absolu du script (pas de slash de fin)
declare -r VERSION="2022.07.22";
#declare CONF_REP="$HOME/.legite/diskorga";


# ########## #
# LEGITE-LIB #
# ########## #
if [[ ! -f "$SCRIPT_REP/legite-lib.sh" ]]
then
    echo "$SCRIPT_REP/legite-lib.sh introuvable -> quit";
    exit 1;
fi
source $SCRIPT_REP/legite-lib.sh;


# ############## #
# FONCTIONS CORE #
# ############## #
source "${SCRIPT_REP}/${SCRIPT_NAME}.core.sh";


# #################### #
# FONCTIONS GENERIQUES #
# #################### #
    function usage(){
        if [[ $argNb -eq 0 ]]
        then
            echo "$SCRIPT_FILENAME [-d] [--update] [--showVars] [--showLibs|--listeCollections] [--conf=fichier.sh] repSource [repDestination='./']";
            echo '-d: Chaque presence de ce parametre augmente le niveau de verbosité du debug.'
            echo "--update: update le programme legite-install.sh lui meme (via $UPDATE_HTTP)."
            echo '--conf: execute un fichier de configuration si existe.'
            echo "Fichier de configurations:      (visible avec --showVars)."
            echo "programme: execute une fonction (visible avec --showLibs)."
            echo -e "\nCreation du cache (copie)"
            echo "[--cachePrefixe] [--dironly] [--fileonly] [--fichierMotif=motif] --createCache repertoire_source";
            echo '--dironly: active le mode recursif(-R)'
            echo -e "\nRenommer"
            echo "[-R] [--profondeur_max=n] [--dironly] [--fileonly] [--fichierMotif=motif] --rename repertoire_source";
            echo -e "\nCopier"
            echo "[-R] [--profondeur_max=n] [--dironly] [--fileonly] [--keepCache] [--cachePrefixe] [--fromCache] [--fichierMotif=motif] --copy repertoire_source repertoire_destination";
            echo -e "\nComparer"
            echo "[-R] --compareStats: Comparer les stats de repertoire_source et repertoire_destination";
            echo -e "\nConvertir (via ffmpeg)"
            echo "[-R] --convert [--convertFrom=m4a] [--convertTo=ogg] [repertoire='./']";
            echo -e "\nCommandes multiples:"
            echo "Si plusieurs commandes sont indiquées simultanément elles sont faites dans l'ordre suivant: renommer, copier, comparer"
            echo " --test filtre";
            echo " --profondeur_max 0:desactiver; 1: repertoire_source; n: root+n-1. defaut: $PROFONDEUR_MAX"
            echo "fichierMotif=\".m4a$\\.ogg$\" # ne recherche que les extentions m4a et ogg"
            echo 'repSource et repDestination sont le chemin courant "./" par d"faut'
            exit $E_ARG_NONE;
        fi
    }


    function showVarsLocal(){
        fctIn "$*"
        displayVar "repertoire_source" "$repertoire_source"
        displayVar "repertoire_destination" "$repertoire_destination"
        displayVar "fichierMotif" "$fichierMotif"
        displayVar "REP_TEMP" "$REP_TEMP"
        displayVar "cachePrefixe" "$cachePrefixe" "isCreateCache" "$isCreateCache" "isKeepCache" "$isKeepCache" "isFromCache" "$isFromCache"
        displayVar "isTest" "$isTest"
        displayVar "isDeleted" "$isDeleted"
        displayVar "isRecursif" "$isRecursif" "isDironly" "$isDironly" "isFileonly" "$isFileonly"
        displayVar "isRename" "$isRename" "isCopie" "$isCopie" "isCompare" "$isCompare" 'isCompareStats' "$isCompareStats"
        displayVar "isConvert" "$isConvert" "convertFrom" "$convertFrom" "convertTo" "$convertTo"
        displayVar "isStats" "$isStats"
        displayVar "tabulationMotif" "$tabulationMotif" "tabulation" "$tabulation"
        displayVar "repertoireTotal" "$repertoireTotal"
        displayVar "profondeurNb" "$profondeurNb" "PROFONDEUR_MAX" "$PROFONDEUR_MAX"
        fctOut "$FUNCNAME";return 0;
    }

    function showVarsPostLocal(){
        fctIn "$*"
        fctOut "$FUNCNAME";return 0;
    }
#


################
# MISE A JOURS #
################
function selfUpdateLocal(){
    fctIn "$*"
    if [[ $isUpdate -eq 1 ]]
    then
        notifsAddEcho "mise a jours specifique au programme";

        if [[ $IS_ROOT -eq 1  ]]
        then
            displayDebug ''
        fi

    fi
    fctOut "$FUNCNAME";return 0;
}


# ########## #
# PARAMETRES #
# ########## #
TEMP=$(getopt \
     --options v::dVhnpRc:: \
     --long help,version,verbose::,debug,showVars,showCollections,update,conf::,showLibs,showDuree\
,profondeur_max::,dironly,fileonly,keepCache,fromCache,cachePrefixe::,fichierMotif::,compareStats\
,rename,copy,compare,convert,convertFrom::,convertTo::,stats,createCache\
     -- "$@")
    eval set -- "$TEMP"

    while true
    do
        if [ $isTrapCAsk -eq 1 ];then break;fi
        argument=${1:-''}
        parametre=${2:-''}
        #displayVarDebug 'argument' "$argument" 'parametre' "$parametre"
        case "$argument" in

            # - fonctions generiques - #
            -h|--help)    usage; exit 0;                shift ;;
            -V|--version) echo "$VERSION"; exit 0;      shift ;;
            -v|--verbose)
                case "$parametre" in
                    '') verbosity=1; shift 2 ;;
                    *)  #echo "Option c, argument '$2'" ;
                    verbosity=$parametre; shift 2;;
                esac
                ;;
            -d|--debug) ((isDebug++));isShowVars=1;         shift  ;;
            --update)   isUpdate=1;                     shift ;;

            --showVars)         isShowVars=1;           shift  ;;
            --showCollections)  isShowCollections=1;    shift  ;;
            --showLibs )        isShowLibs=1;           shift  ;;
            --showDuree )       isShowDuree=1;          shift  ;;

            --conf)     CONF_PSP_PATH="$parametre";  shift 2; ;;

            -S)
                isSimulation=1;
                YTDL_PARAM_GLOBAL="${YTDL_PARAM_GLOBAL} --simulate"
                shift
                ;;

            --collection)       collection=$parametre; shift 2 ;;


            # - parametres liés au projet - #
            #--createTest) isCreateTest=1; createTest; shift; ;;
            #--test)       isTest=1;       lanceTest;  shift; ;;
            --rename)     isRename=1;     shift; ;;
            --keepCache)  isKeepCache=1;  shift; ;;

            --createCache)
                isCreateCache=1;
                isKeepCache=1; # force la preservation du cache
                shift;
                ;;

            --cachePrefixe) cachePrefixe="$2";  shift 2; ;;
            --fromCache)    isFromCache=1;      shift;   ;;
            -c|--copy)      isCopie=1;isCompareStats=1;  shift;   ;;
            --compare)      isCompare=1;        shift;   ;;
            --convert)      isConvert=1;        shift;   ;;
            --convertFrom)  convertFrom="$2";   shift 2; ;;
            --convertTo)    convertTo="$2";     shift 2; ;;

            -R)             isRecursif=1;       shift;   ;;
            --dironly)      isDironly=1;        isRecursif=1;      shift;   ;;
            --fileonly)     isFileonly=1;       shift;   ;;
            --stats)        isStats=1;          shift;   ;;
            --compareStats) isCompareStats=1;   shift;   ;;

            -p|--profondeur_max )
                # on accepte la valeur demandé
                PROFONDEUR_MAX=$2;

                # on limite les valeurs si elle depasse
                if [ $2 -lt 0 ]
                then
                    PROFONDEUR_MAX=0;
                    titreWarn "PROFONDEUR_MAX=$PROFONDEUR_MAX"
                fi
                if [ $2 -gt 9 ]
                then
                    PROFONDEUR_MAX=9;
                    titreWarn "PROFONDEUR_MAX=$PROFONDEUR_MAX"
                fi
                shift 2
                ;;

    
            --fichierMotif)
                #fichierMotif="\"$2\"";
                fichierMotif="$2"
                shift 2
                ;;

            # - Les parametres supplementaires - #
            --)
                repertoire_source="${2:-"./"}"
                repertoire_destination=${3:-"./"}
                shift;
                break
                ;;
            
            *)
                repertoire_source="${1:-"./"}"
                repertoire_destination=${2:-"./"}
                shift;
                break
        esac
    done
#


########
# main #
########

beginStandard


if [[ "$repertoire_source" == '' ]]
then
    erreursAddEcho 'Une source doit etre defini.'
    endStandard
    exit $E_ARG_BAD;
fi


if [[ ! -d "$repertoire_source" ]]
then
    titreWarn "Le repertoire source '$repertoire_source' n'est pas un repertoire!"
    endStandard
    exit $E_ARG_BAD;
else
    displayDebug "'$repertoire_source' est bien un repertoire." $LINENO;
fi


repertoire_source=${repertoire_source%/}
repertoire_source="$repertoire_source/"
repertoire_destination=${repertoire_destination%/}
repertoire_destination="$repertoire_destination/"

displayVar "repertoire_source" "$repertoire_source"
displayVar "repertoire_destination" "$repertoire_destination"


if [[ $isCreateCache -eq 1 ]]
then
    createCache 'd'
    createCache 'f'
fi

if [[ $isRename -eq 1 ]]
then
    titre1 'RENAME'
    renameRep "$repertoire_source"
fi

if [[ $isCopie -eq 1 ]]
then
    copier
fi

if [[ $isCompare -eq 1 ]]
then
    compare
fi

if [[ $isConvert -eq 1 ]]
then
    convert
fi

if [ $isKeepCache -eq 0 -a -f "$cachePrefixe-dir.txt" ]
then
    rm "$cachePrefixe-dir.txt";
fi

if [[ $isStats -eq 1 ]]
then
    evalCmd "stats_analyse '$repertoire_source'"
fi


endStandard

resumer # apres la sortie standard

#echo "FIN NORMAL"
if [[ $isTrapCAsk -eq 1 ]];then exit $E_CTRL_C;fi
exit 0 # https://abs.traduc.org/abs-fr/apd.html#exitcodesref
